package hu.rmatesz.movieaddict.util;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */
public class EmailValidatorTest {

    private final static String EMAIL_VALID_1 = "email@example.com";
    private final static String EMAIL_VALID_2 = "firstname.lastname@example.com";
    private final static String EMAIL_VALID_3 = "email@subdomain.example.com";
    private final static String EMAIL_VALID_4 = "firstname+lastname@example.com";
    private final static String EMAIL_VALID_5 = "1234567890@example.com";
    private final static String EMAIL_VALID_6 = "email@example-one.com";
    private final static String EMAIL_VALID_7 = "_______@example.com";
    private final static String EMAIL_VALID_8 = "email@example.name";
    private final static String EMAIL_VALID_9 = "email@example.museum";
    private final static String EMAIL_VALID_10 = "email@example.co.jp";
    private final static String EMAIL_VALID_11 = "firstname-lastname@example.com";
    private final static String EMAIL_INVALID_EMPTY = "";
    private final static String EMAIL_INVALID_PLAIN = "plainaddress";
    private final static String EMAIL_INVALID_1 = "#@%^%#$@#$@#.com";
    private final static String EMAIL_INVALID_2 = "@example.com";
    private final static String EMAIL_INVALID_3 = "Joe Smith <email@example.com>";
    private final static String EMAIL_INVALID_4 = "email.example.com";
    private final static String EMAIL_INVALID_5 = "email@example@example.com";
    private final static String EMAIL_INVALID_6 = ".email@example.com";
    private final static String EMAIL_INVALID_7 = "email.@example.com";
    private final static String EMAIL_INVALID_8 = "email..email@example.com";
    private final static String EMAIL_INVALID_9 = "あいうえお@example.com";
    private final static String EMAIL_INVALID_10 = "email@example.com (Joe Smith)";
    private final static String EMAIL_INVALID_11 = "email@example";
    private final static String EMAIL_INVALID_12 = "email@-example.com";
    private final static String EMAIL_INVALID_13 = "email@111.222.333.44444";
    private final static String EMAIL_INVALID_14 = "email@example..com";
    private final static String EMAIL_INVALID_15 = "test.email@test.c";

    private EmailValidator underTest;

    @Before
    public void setUp() {
        underTest = new EmailValidator();
    }

    @Test
    public void validateEmailWhenValid1() throws Exception {
        EmailValidator.ValidationResult result =underTest.validateEmail(EMAIL_VALID_1);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid2() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_2);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid3() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_3);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid4() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_4);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid5() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_5);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid6() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_6);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid7() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_7);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid8() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_8);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid9() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_9);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid10() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_10);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenValid11() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_VALID_11);

        Assert.assertEquals(EmailValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validateEmailWhenEmpty() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_EMPTY);

        Assert.assertEquals(EmailValidator.ValidationResult.EMPTY, result);
    }

    @Test
    public void validateEmailWhenPlain() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_PLAIN);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError1() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_1);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError2() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_2);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError3() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_3);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError4() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_4);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError5() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_5);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError6() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_6);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError7() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_7);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError8() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_8);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError9() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_9);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError10() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_10);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError11() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_11);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError12() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_12);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError13() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_13);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError14() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_14);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }

    @Test
    public void validateEmailWhenError15() throws Exception {
        EmailValidator.ValidationResult result = underTest.validateEmail(EMAIL_INVALID_15);

        Assert.assertEquals(EmailValidator.ValidationResult.INVALID_FORMAT, result);
    }
}