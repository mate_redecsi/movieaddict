package hu.rmatesz.movieaddict.util;

import android.content.res.TestTypedArray;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by Mate_Redecsi on 12/28/2016.
 */
//@RunWith(RobolectricTestRunner.class)
//@Config(constants = BuildConfig.class, sdk = 23)
public class ResourcesUtilTest {
    private ResourcesUtil underTest;

    @Before
    public void setUp() {
        underTest = new ResourcesUtil();
    }

    @Test
    public void getResourceList() throws Exception {
        TestTypedArray resources = new TestTypedArray(new int[] {1,2,3,4,5});
//        https://github.com/robolectric/robolectric/issues/1283
//        Robolectric issue: obtainTypedArray always returns empty array.
//        TypedArray resources = RuntimeEnvironment.application.getResources().obtainTypedArray(R.array.test_resource_list);
        List<Integer> resourceList = underTest.getResourceList(resources);
        Assert.assertEquals(5, resourceList.size());
        Assert.assertEquals(1, resourceList.get(0).intValue());
        Assert.assertEquals(2, resourceList.get(1).intValue());
        Assert.assertEquals(3, resourceList.get(2).intValue());
        Assert.assertEquals(4, resourceList.get(3).intValue());
        Assert.assertEquals(5, resourceList.get(4).intValue());

    }

}