package hu.rmatesz.movieaddict.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */
public class PasswordValidatorTest {
    private final static String PASSWORD_EMPTY = "";
    private final static String PASSWORD_TOO_SHORT = "asdfg";
    private final static String PASSWORD_VALID = "asdfgasdfg";

    private PasswordValidator underTest;

    @Before
    public void setUp() throws Exception {
        underTest = new PasswordValidator();
    }

    @Test
    public void validatePasswordWhenValid() throws Exception {
        PasswordValidator.ValidationResult result = underTest.validatePassword(PASSWORD_VALID);
        Assert.assertEquals(PasswordValidator.ValidationResult.SUCCESS, result);
    }

    @Test
    public void validatePasswordWhenEmpty() throws Exception {
        PasswordValidator.ValidationResult result = underTest.validatePassword(PASSWORD_EMPTY);
        Assert.assertEquals(PasswordValidator.ValidationResult.EMPTY, result);
    }

    @Test
    public void validatePasswordWhenTooShort() throws Exception {
        PasswordValidator.ValidationResult result = underTest.validatePassword(PASSWORD_TOO_SHORT);
        Assert.assertEquals(PasswordValidator.ValidationResult.TOO_SHORT, result);
    }

}