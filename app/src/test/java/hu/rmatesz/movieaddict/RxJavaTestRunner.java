package hu.rmatesz.movieaddict;

import org.junit.runners.model.InitializationError;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;

import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.plugins.RxJavaPlugins;
import rx.plugins.RxJavaSchedulersHook;
import rx.schedulers.TestScheduler;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */

public class RxJavaTestRunner extends MockitoJUnitRunner {
    private static TestScheduler testScheduler;

    public RxJavaTestRunner(Class<?> testClass) throws InitializationError, InvocationTargetException {
        super(testClass);

        testScheduler = new TestScheduler();

        RxJavaPlugins.getInstance().reset();
        RxJavaPlugins.getInstance().registerSchedulersHook(new RxJavaSchedulersHook() {
            @Override
            public Scheduler getComputationScheduler() {
                return testScheduler;
            }
        });

        RxAndroidPlugins.getInstance().reset();
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {

            @Override
            public Scheduler getMainThreadScheduler() {
                return testScheduler;
            }
        });
    }

    public static TestScheduler getTestScheduler() {
        return testScheduler;
    }
}
