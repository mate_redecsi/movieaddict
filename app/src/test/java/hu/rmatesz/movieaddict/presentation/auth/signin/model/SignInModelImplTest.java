package hu.rmatesz.movieaddict.presentation.auth.signin.model;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import hu.rmatesz.movieaddict.RxJavaTestRunner;
import hu.rmatesz.movieaddict.data.auth.model.User;
import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Mate_Redecsi on 12/31/2016.
 */
@RunWith(RxJavaTestRunner.class)
public class SignInModelImplTest {
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final User USER = new User(EMAIL, PASSWORD);
    private static final Throwable ERROR = new Throwable();

    private SignInModelImpl underTest;
    @Mock
    private AuthenticationDataProvider authenticationDataProviderMock;
    @Mock
    private SignInModel.Callback callbackMock;

    @Before
    public void setUp() {
        underTest = new SignInModelImpl(authenticationDataProviderMock);
    }

    @Test
    public void signInWhenSuccess() throws Exception {
        underTest.registerCallback(callbackMock);
        when(authenticationDataProviderMock.signIn(EMAIL, PASSWORD)).thenReturn(Observable.just(USER));
        underTest.signIn(EMAIL, PASSWORD);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSuccessfulSignIn();
        assertNull(underTest.pendingSignInResult);
    }

    @Test
    public void signInWhenError() throws Exception {
        underTest.registerCallback(callbackMock);
        when(authenticationDataProviderMock.signIn(EMAIL, PASSWORD)).thenReturn(Observable.error(ERROR));
        underTest.signIn(EMAIL, PASSWORD);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSignInError();
        assertNull(underTest.pendingSignInError);
    }

    @Test
    public void signInWhenCallbackNotRegistered() throws Exception {
        when(authenticationDataProviderMock.signIn(EMAIL, PASSWORD)).thenReturn(Observable.just(USER));
        underTest.signIn(EMAIL, PASSWORD);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock, never()).onSuccessfulSignIn();
        assertEquals(USER, underTest.pendingSignInResult);
    }

    @Test
    public void signInWhenErrorAndCallbackNotRegistered() throws Exception {
        when(authenticationDataProviderMock.signIn(EMAIL, PASSWORD)).thenReturn(Observable.error(ERROR));
        underTest.signIn(EMAIL, PASSWORD);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock, never()).onSignInError();
        assertEquals(ERROR, underTest.pendingSignInError);
    }

    @Test
    public void signInWhenAlreadySubscribed() {
        underTest.signInSubscription = new TestSubscriber<>();
        underTest.signIn(EMAIL, PASSWORD);
        verify(authenticationDataProviderMock, never()).signIn(any(), any());
    }

    @Test
    public void stopWhenSubscribed() throws Exception {
        underTest.signInSubscription = new TestSubscriber<>();
        underTest.stop();
        Assert.assertTrue(underTest.signInSubscription.isUnsubscribed());
    }

    @Test
    public void stopWhenSubscriptionIsNull() throws Exception {
        underTest.stop();
        Assert.assertNull(underTest.signInSubscription);
    }

    @Test
    public void stopWhenUnsubscribed() throws Exception {
        underTest.signInSubscription = new TestSubscriber<>();
        underTest.signInSubscription.unsubscribe();
        underTest.stop();
        Assert.assertTrue(underTest.signInSubscription.isUnsubscribed());
    }

    @Test
    public void testRegisterCallback() throws Exception {
        underTest.registerCallback(callbackMock);
        assertEquals(1, underTest.callbacks.size());
        assertEquals(callbackMock, underTest.callbacks.get(0));
    }

    @Test
    public void registerCallbackWhenPendingSignInResult() throws Exception {
        underTest.pendingSignInResult = USER;
        underTest.registerCallback(callbackMock);
        verify(callbackMock).onSuccessfulSignIn();
    }

    @Test
    public void registerCallbackWhenPendingSignInError() throws Exception {
        underTest.pendingSignInError = ERROR;
        underTest.registerCallback(callbackMock);
        verify(callbackMock).onSignInError();
    }

    @Test
    public void testUnegisterCallback() throws Exception {
        underTest.registerCallback(callbackMock);
        underTest.unregisterCallback(callbackMock);
        assertEquals(0, underTest.callbacks.size());
    }

}