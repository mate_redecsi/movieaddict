package hu.rmatesz.movieaddict.presentation.auth.signin.viewmodel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import hu.rmatesz.movieaddict.TestSpannableString;
import hu.rmatesz.movieaddict.presentation.auth.signin.model.SignInModel;
import hu.rmatesz.movieaddict.presentation.auth.signin.router.SignInRouter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * Created by Mate_Redecsi on 1/1/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class SignInViewModelImplTest {
    private final String EMAIL = "email";
    private final String PASSWORD = "password";

    private SignInViewModelImpl underTest;

    @Mock
    private SignInRouter routerMock;
    @Mock
    private SignInModel modelMock;

    @Before
    public void setUp() throws Exception {
        underTest = new SignInViewModelImpl(routerMock, modelMock);
    }

    @Test
    public void onStart() throws Exception {
        underTest.onStart();
        verify(modelMock).registerCallback(underTest);
    }

    @Test
    public void onStop() throws Exception {
        underTest.onStop();
        verify(modelMock).unregisterCallback(underTest);
    }

    @Test
    public void onSignInClicked() throws Exception {
        underTest.email = EMAIL;
        underTest.password = PASSWORD;
        underTest.onSignInClicked(null);
        verify(modelMock).signIn(EMAIL, PASSWORD);
    }

    @Test
    public void onSignUpClicked() throws Exception {
        underTest.onSignUpClicked(null);
        verify(routerMock).openSignUp();
    }

    @Test
    public void onEmailChanged() throws Exception {
        underTest.onEmailChanged(new TestSpannableString(EMAIL));
        assertEquals(EMAIL, underTest.getEmail());
    }

    @Test
    public void onPasswordChanged() throws Exception {
        underTest.onPasswordChanged(new TestSpannableString(PASSWORD));
        assertEquals(PASSWORD, underTest.getPassword());
    }

    @Test
    public void onSuccessfulSignIn() throws Exception {
        underTest.onSuccessfulSignIn();
        verify(routerMock).displaySnackbarMessage(any());
        verify(routerMock).openStartPage();
    }

    @Test
    public void onSignInError() throws Exception {
        underTest.onSignInError();
        verify(routerMock).displayErrorDialog(any(), any());
    }

}