package hu.rmatesz.movieaddict.presentation.auth.signup.viewmodel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import hu.rmatesz.movieaddict.TestSpannableString;
import hu.rmatesz.movieaddict.presentation.auth.signup.model.SignUpModel;
import hu.rmatesz.movieaddict.presentation.auth.signup.router.SignUpRouter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * Created by Mate_Redecsi on 12/28/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class SignUpViewModelImplTest {
    private final String EMAIL = "email";
    private final String PASSWORD = "password";
    private final String PASSWORD_CONFIRM = "password_confirm";
    private final String EMAIL_ERROR = "email_error";
    private final String PASSWORD_ERROR = "password_error";

    private SignUpViewModelImpl underTest;

    @Mock
    private SignUpRouter routerMock;
    @Mock
    private SignUpModel modelMock;

    @Before
    public void setUp() throws Exception {
        underTest = new SignUpViewModelImpl(routerMock, modelMock);
    }

    @Test
    public void onStart() throws Exception {
        underTest.onStart();
        verify(modelMock).registerCallback(underTest);
    }

    @Test
    public void onStop() throws Exception {
        underTest.onStop();
        verify(modelMock).unregisterCallback(underTest);
    }

    @Test
    public void onSignUpClicked() throws Exception {
        underTest.email = EMAIL;
        underTest.password = PASSWORD;
        underTest.onSignUpClicked(null);
        verify(modelMock).signUp(EMAIL, PASSWORD);
    }

    @Test
    public void onEmailChanged() throws Exception {
        underTest.onEmailChanged(new TestSpannableString(EMAIL));
        assertEquals(EMAIL, underTest.getEmail());
        verify(modelMock).validateEmail(EMAIL);
    }

    @Test
    public void onPasswordChanged() throws Exception {
        underTest.onPasswordChanged(new TestSpannableString(PASSWORD));
        assertEquals(PASSWORD, underTest.getPassword());
        verify(modelMock).validatePassword(PASSWORD);
    }

    @Test
    public void onConfirmPasswordChanged() throws Exception {
        underTest.password = PASSWORD;
        underTest.onConfirmPasswordChanged(new TestSpannableString(PASSWORD_CONFIRM));
        verify(modelMock).validateConfirmPassword(PASSWORD, PASSWORD_CONFIRM);
    }

    @Test
    public void onSuccessfulSignUp() throws Exception {
        underTest.onSuccessfulSignUp();
        verify(routerMock).displaySnackbarMessage(any());
        verify(routerMock).openStartPage();
    }

    @Test
    public void onSignUpError() throws Exception {
        underTest.onSignUpError();
        verify(routerMock).displayErrorDialog(any(), any());
    }

    @Test
    public void onEmailValidationError() throws Exception {
        underTest.onEmailValidationError(SignUpModel.EmailError.EMPTY);
        assertNotNull(underTest.getEmailError());
        assertNotEquals("", underTest.getEmailError());
    }

    @Test
    public void onPasswordValidationError() throws Exception {
        underTest.onPasswordValidationError(SignUpModel.PasswordError.EMPTY);
        assertNotNull(underTest.getPasswordError());
        assertNotEquals("", underTest.getPasswordError());
    }

    @Test
    public void onConfirmPasswordValidationError() throws Exception {
        underTest.onConfirmPasswordValidationError();
        assertNotNull(underTest.getConfirmPasswordError());
        assertNotEquals("", underTest.getConfirmPasswordError());
    }

}