package hu.rmatesz.movieaddict.presentation.search.model;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import hu.rmatesz.movieaddict.RxJavaTestRunner;
import hu.rmatesz.movieaddict.api.model.Movie;
import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.data.movies.provider.MoviesDataProvider;
import hu.rmatesz.movieaddict.data.series.provider.SeriesDataProvider;
import rx.Observable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Mate_Redecsi on 1/1/2017.
 */
@RunWith(RxJavaTestRunner.class)
public class SearchModelImplTest {
    private static final String TITLE = "title";
    private static final Series SERIES_1 = new Series();
    private static final Series SERIES_2 = new Series();
    private static final Series SERIES_3 = new Series();
    private static final Series SERIES_4 = new Series();
    private static final Series SERIES_5 = new Series();
    private static final Series SERIES_6 = new Series();
    private static final Series SERIES_7 = new Series();
    private static final Series SERIES_8 = new Series();
    private static final Series SERIES_9 = new Series();
    private static final List<Series> SERIES_PAGE_1 = Lists.newArrayList(SERIES_1,SERIES_2,SERIES_3,SERIES_4,SERIES_5);
    private static final List<Series> SERIES_PAGE_2 = Lists.newArrayList(SERIES_6,SERIES_7,SERIES_8,SERIES_9);
    private static final List<Series> SERIES_ALL = new ArrayList<>();
    private static final Throwable ERROR = new Throwable();

    static {
        SERIES_ALL.addAll(SERIES_PAGE_1);
        SERIES_ALL.addAll(SERIES_PAGE_2);
    }
    private static final Movie MOVIE_1 = new Movie();
    private static final Movie MOVIE_2 = new Movie();
    private static final Movie MOVIE_3 = new Movie();
    private static final Movie MOVIE_4 = new Movie();
    private static final Movie MOVIE_5 = new Movie();
    private static final Movie MOVIE_6 = new Movie();
    private static final Movie MOVIE_7 = new Movie();
    private static final Movie MOVIE_8 = new Movie();
    private static final Movie MOVIE_9 = new Movie();
    private static final List<Movie> MOVIES_PAGE_1 = Lists.newArrayList(MOVIE_1,MOVIE_2,MOVIE_3,MOVIE_4,MOVIE_5);
    private static final List<Movie> MOVIES_PAGE_2 = Lists.newArrayList(MOVIE_6,MOVIE_7,MOVIE_8,MOVIE_9);
    private static final List<Movie> MOVIES_ALL = new ArrayList<>();
    static {
        MOVIES_ALL.addAll(MOVIES_PAGE_1);
        MOVIES_ALL.addAll(MOVIES_PAGE_2);
    }

    private SearchModelImpl underTest;

    @Mock
    private SeriesDataProvider seriesDataProviderMock;
    @Mock
    private MoviesDataProvider moviesDataProviderMock;
    @Mock
    private SearchModel.Callback callbackMock;

    @Before
    public void setUp() throws Exception {
        underTest = new SearchModelImpl(seriesDataProviderMock, moviesDataProviderMock);
    }

    @Test
    public void testSearchWhenSearchStarted() {
        underTest.registerCallback(callbackMock);
        when(seriesDataProviderMock.searchSeries(TITLE)).thenReturn(Observable.just(null));
        when(moviesDataProviderMock.searchMovies(TITLE)).thenReturn(Observable.just(null));
        underTest.search(TITLE);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSearchStarted();
    }

    @Test
    public void testSearchWhenSearchSeriesFinished() {
        underTest.registerCallback(callbackMock);
        when(seriesDataProviderMock.searchSeries(TITLE)).thenReturn(Observable.just(SERIES_PAGE_1));
        when(moviesDataProviderMock.searchMovies(TITLE)).thenReturn(Observable.just(null));
        underTest.search(TITLE);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSearchSeriesFinished();
    }

    @Test
    public void testSearchWhenSearchMoviesFinished() {
        underTest.registerCallback(callbackMock);
        when(seriesDataProviderMock.searchSeries(TITLE)).thenReturn(Observable.just(null));
        when(moviesDataProviderMock.searchMovies(TITLE)).thenReturn(Observable.just(MOVIES_PAGE_1));
        underTest.search(TITLE);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSearchMoviesFinished();
    }

    @Test
    public void searchWhenOnePageResult() throws Exception {
        underTest.registerCallback(callbackMock);
        when(seriesDataProviderMock.searchSeries(TITLE)).thenReturn(Observable.just(SERIES_PAGE_1));
        when(moviesDataProviderMock.searchMovies(TITLE)).thenReturn(Observable.just(MOVIES_PAGE_1));
        underTest.search(TITLE);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSearchResultArrived(SERIES_PAGE_1.toArray(new Series[SERIES_PAGE_1.size()]));
        verify(callbackMock).onSearchResultArrived(MOVIES_PAGE_1.toArray(new Movie[MOVIES_PAGE_1.size()]));
    }

    @Test
    public void searchWhenTwoPageResult() throws Exception {
        underTest.registerCallback(callbackMock);
        when(seriesDataProviderMock.searchSeries(TITLE)).thenReturn(Observable.just(SERIES_PAGE_1, SERIES_PAGE_2));
        when(moviesDataProviderMock.searchMovies(TITLE)).thenReturn(Observable.just(MOVIES_PAGE_1, MOVIES_PAGE_2));
        underTest.search(TITLE);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSearchResultArrived(SERIES_PAGE_1.toArray(new Series[SERIES_PAGE_1.size()]));
        verify(callbackMock).onSearchResultArrived(MOVIES_PAGE_1.toArray(new Movie[MOVIES_PAGE_1.size()]));
        verify(callbackMock).onSearchResultArrived(SERIES_ALL.toArray(new Series[SERIES_ALL.size()]));
        verify(callbackMock).onSearchResultArrived(MOVIES_ALL.toArray(new Movie[MOVIES_ALL.size()]));
    }

    @Test
    public void searchWhenSeriesError() throws Exception {
        underTest.registerCallback(callbackMock);
        when(seriesDataProviderMock.searchSeries(TITLE)).thenReturn(Observable.error(ERROR));
        when(moviesDataProviderMock.searchMovies(TITLE)).thenReturn(Observable.just(MOVIES_PAGE_1));
        underTest.search(TITLE);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSearchError();
    }

    @Test
    public void loadNextSeriesPage() throws Exception {
        underTest.loadNextSeriesPage();
        verify(seriesDataProviderMock).loadPage();
    }

    @Test
    public void loadNextMoviesPage() throws Exception {
        underTest.loadNextMoviesPage();
        verify(moviesDataProviderMock).loadPage();
    }

    @Test
    public void stop() throws Exception {

    }

    @Test
    public void registerCallback() throws Exception {

    }

    @Test
    public void unregisterCallback() throws Exception {

    }

}