package hu.rmatesz.movieaddict.presentation.auth.signup.model;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import hu.rmatesz.movieaddict.RxJavaTestRunner;
import hu.rmatesz.movieaddict.data.auth.model.User;
import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;
import hu.rmatesz.movieaddict.util.EmailValidator;
import hu.rmatesz.movieaddict.util.PasswordValidator;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */
@RunWith(RxJavaTestRunner.class)
public class SignUpModelImplTest {
    private final static String EMAIL = "email";
    private final static String PASSWORD = "password";
    private final static String PASSWORD_SAME = "password";
    private final static String PASSWORD_OTHER = "passwordother";
    private final static User USER = new User(EMAIL, PASSWORD);
    private final static Throwable ERROR = new Throwable();

    private SignUpModelImpl underTest;
    @Mock
    private EmailValidator emailValidatorMock;
    @Mock
    private PasswordValidator passwordValidatorMock;
    @Mock
    private AuthenticationDataProvider authenticationDataProviderMock;
    @Mock
    private SignUpModel.Callback callbackMock;

    @Before
    public void setUp() {
        underTest = new SignUpModelImpl(authenticationDataProviderMock, emailValidatorMock, passwordValidatorMock);
    }

    @Test
    public void validateEmailWhenSuccess() {
        underTest.registerCallback(callbackMock);
        when(emailValidatorMock.validateEmail(EMAIL)).thenReturn(EmailValidator.ValidationResult.SUCCESS);
        underTest.validateEmail(EMAIL);
        verify(callbackMock, never()).onEmailValidationError(any());
    }

    @Test
    public void validateEmailWhenEmpty() {
        underTest.registerCallback(callbackMock);
        when(emailValidatorMock.validateEmail(EMAIL)).thenReturn(EmailValidator.ValidationResult.EMPTY);
        underTest.validateEmail(EMAIL);
        verify(callbackMock).onEmailValidationError(SignUpModel.EmailError.EMPTY);
        assertNull(underTest.pendingEmailError);
    }

    @Test
    public void validateEmailWhenEmptyAndNoCallback() {
        when(emailValidatorMock.validateEmail(EMAIL)).thenReturn(EmailValidator.ValidationResult.EMPTY);
        underTest.validateEmail(EMAIL);
        verify(callbackMock, never()).onEmailValidationError(SignUpModel.EmailError.EMPTY);
        assertEquals(SignUpModel.EmailError.EMPTY, underTest.pendingEmailError);
    }

    @Test
    public void validateEmailWhenInvalidFormat() {
        underTest.registerCallback(callbackMock);
        when(emailValidatorMock.validateEmail(EMAIL)).thenReturn(EmailValidator.ValidationResult.INVALID_FORMAT);
        underTest.validateEmail(EMAIL);
        verify(callbackMock).onEmailValidationError(SignUpModel.EmailError.INVALID_FORMAT);
    }

    @Test
    public void validatePasswordWhenSuccess() throws Exception {
        underTest.registerCallback(callbackMock);
        when(passwordValidatorMock.validatePassword(PASSWORD)).thenReturn(PasswordValidator.ValidationResult.SUCCESS);
        underTest.validatePassword(PASSWORD);
        verify(callbackMock, never()).onPasswordValidationError(any());
    }

    @Test
    public void validatePasswordWhenEmpty() throws Exception {
        underTest.registerCallback(callbackMock);
        when(passwordValidatorMock.validatePassword(PASSWORD)).thenReturn(PasswordValidator.ValidationResult.EMPTY);
        underTest.validatePassword(PASSWORD);
        verify(callbackMock).onPasswordValidationError(SignUpModel.PasswordError.EMPTY);
        assertNull(underTest.pendingPasswordError);
    }

    @Test
    public void validatePasswordWhenEmptyAndNoCallback() throws Exception {
        when(passwordValidatorMock.validatePassword(PASSWORD)).thenReturn(PasswordValidator.ValidationResult.EMPTY);
        underTest.validatePassword(PASSWORD);
        verify(callbackMock, never()).onPasswordValidationError(SignUpModel.PasswordError.EMPTY);
        assertEquals(SignUpModel.PasswordError.EMPTY, underTest.pendingPasswordError);
    }

    @Test
    public void validatePasswordWhenTooShort() throws Exception {
        underTest.registerCallback(callbackMock);
        when(passwordValidatorMock.validatePassword(PASSWORD)).thenReturn(PasswordValidator.ValidationResult.TOO_SHORT);
        underTest.validatePassword(PASSWORD);
        verify(callbackMock).onPasswordValidationError(SignUpModel.PasswordError.TOO_SHORT);
    }

    @Test
    public void validateConfirmPasswordWhenEquals() throws Exception {
        underTest.registerCallback(callbackMock);
        underTest.validateConfirmPassword(PASSWORD, PASSWORD_SAME);
        verify(callbackMock, never()).onConfirmPasswordValidationError();
    }

    @Test
    public void validateConfirmPasswordWhenDifferent() throws Exception {
        underTest.registerCallback(callbackMock);
        underTest.validateConfirmPassword(PASSWORD, PASSWORD_OTHER);
        verify(callbackMock).onConfirmPasswordValidationError();
        assertFalse(underTest.pendingPasswordMismatchError);
    }

    @Test
    public void validateConfirmPasswordWhenDifferentAndNoCallback() throws Exception {
        underTest.validateConfirmPassword(PASSWORD, PASSWORD_OTHER);
        verify(callbackMock, never()).onConfirmPasswordValidationError();
        assertTrue(underTest.pendingPasswordMismatchError);
    }

    @Test
    public void signUpWhenError() throws Exception {
        underTest.registerCallback(callbackMock);
        when(authenticationDataProviderMock.signUp(EMAIL, PASSWORD)).thenReturn(Observable.error(ERROR));
        underTest.signUp(EMAIL, PASSWORD);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSignUpError();
        assertNull(underTest.pendingSignUpError);
    }

    @Test
    public void signUpWhenErrorAndNoCallback() throws Exception {
        when(authenticationDataProviderMock.signUp(EMAIL, PASSWORD)).thenReturn(Observable.error(ERROR));
        underTest.signUp(EMAIL, PASSWORD);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock, never()).onSignUpError();
        assertEquals(ERROR, underTest.pendingSignUpError);
    }

    @Test
    public void signUpWhenSuccess() throws Exception {
        underTest.registerCallback(callbackMock);
        when(authenticationDataProviderMock.signUp(EMAIL, PASSWORD)).thenReturn(Observable.just(USER));
        underTest.signUp(EMAIL, PASSWORD);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock).onSuccessfulSignUp();
        assertNull(underTest.pendingSuccessfulSignUp);
    }

    @Test
    public void signUpWhenSuccessAndNoCallback() throws Exception {
        when(authenticationDataProviderMock.signUp(EMAIL, PASSWORD)).thenReturn(Observable.just(USER));
        underTest.signUp(EMAIL, PASSWORD);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        verify(callbackMock, never()).onSuccessfulSignUp();
        assertEquals(USER, underTest.pendingSuccessfulSignUp);
    }

    @Test
    public void signUpWhenAlreadySubscribed() {
        underTest.signUpSubscription = new TestSubscriber<>();
        underTest.signUp(EMAIL, PASSWORD);
        verify(authenticationDataProviderMock, never()).signUp(any(), any());
    }

    @Test
    public void stopWhenSubscribed() throws Exception {
        underTest.signUpSubscription = new TestSubscriber<>();
        underTest.stop();
        Assert.assertTrue(underTest.signUpSubscription.isUnsubscribed());
    }

    @Test
    public void stopWhenSubscriptionIsNull() throws Exception {
        underTest.stop();
        Assert.assertNull(underTest.signUpSubscription);
    }

    @Test
    public void stopWhenUnsubscribed() throws Exception {
        underTest.signUpSubscription = new TestSubscriber<>();
        underTest.signUpSubscription.unsubscribe();
        underTest.stop();
        Assert.assertTrue(underTest.signUpSubscription.isUnsubscribed());
    }

    @Test
    public void testRegisterCallback() throws Exception {
        underTest.registerCallback(callbackMock);
        assertEquals(1, underTest.callbacks.size());
        assertEquals(callbackMock, underTest.callbacks.get(0));
    }

    @Test
    public void registerCallbackWhenPendingEmailError() throws Exception {
        underTest.pendingEmailError = SignUpModel.EmailError.EMPTY;
        underTest.registerCallback(callbackMock);
        verify(callbackMock).onEmailValidationError(SignUpModel.EmailError.EMPTY);
    }

    @Test
    public void registerCallbackWhenPendingPasswordError() throws Exception {
        underTest.pendingPasswordError = SignUpModel.PasswordError.EMPTY;
        underTest.registerCallback(callbackMock);
        verify(callbackMock).onPasswordValidationError(SignUpModel.PasswordError.EMPTY);
    }

    @Test
    public void registerCallbackWhenPendingPasswordMismatchError() throws Exception {
        underTest.pendingPasswordMismatchError = true;
        underTest.registerCallback(callbackMock);
        verify(callbackMock).onConfirmPasswordValidationError();
    }

    @Test
    public void registerCallbackWhenPendingSuccessfulSignUp() throws Exception {
        underTest.pendingSuccessfulSignUp = USER;
        underTest.registerCallback(callbackMock);
        verify(callbackMock).onSuccessfulSignUp();
    }

    @Test
    public void registerCallbackWhenPendingSignUpError() throws Exception {
        underTest.pendingSignUpError = ERROR;
        underTest.registerCallback(callbackMock);
        verify(callbackMock).onSignUpError();
    }

    @Test
    public void unregisterCallback() throws Exception {
        underTest.registerCallback(callbackMock);
        underTest.unregisterCallback(callbackMock);
        assertEquals(0, underTest.callbacks.size());
    }

}