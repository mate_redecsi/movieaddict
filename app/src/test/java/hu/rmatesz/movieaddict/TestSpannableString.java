package hu.rmatesz.movieaddict;

import android.text.SpannableStringBuilder;

/**
 * Created by Mate_Redecsi on 12/28/2016.
 */

public class TestSpannableString extends SpannableStringBuilder {
    private final String content;
    public TestSpannableString(CharSequence source) {
        super(source);
        content = source.toString();
    }

    @Override
    public String toString() {
        return content;
    }
}
