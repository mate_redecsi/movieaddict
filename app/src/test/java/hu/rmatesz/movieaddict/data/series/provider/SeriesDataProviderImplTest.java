package hu.rmatesz.movieaddict.data.series.provider;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;

import hu.rmatesz.movieaddict.RxJavaTestRunner;
import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.api.service.OmdbService;
import hu.rmatesz.movieaddict.api.service.response.SearchResponse;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by Mate_Redecsi on 12/31/2016.
 */
@RunWith(RxJavaTestRunner.class)
public class SeriesDataProviderImplTest {
    private static final String KEYWORD = "keyword";
    private static final String IMDB_ID = "imdbID";
    private static final Series SERIES_1 = new Series();
    private static final Series SERIES_2 = new Series();
    private static final Series SERIES_3 = new Series();
    private static final Series SERIES_4 = new Series();
    private static final Series SERIES_5 = new Series();
    private static final Series SERIES_6 = new Series();
    private static final Series SERIES_7 = new Series();
    private static final Series SERIES_8 = new Series();
    private static final Series SERIES_9 = new Series();
    private static final List<Series> SERIES_PAGE_1 = Lists.asList(SERIES_1, new Series[]{SERIES_2,SERIES_3,SERIES_4,SERIES_5});
    private static final List<Series> SERIES_PAGE_2 = Lists.asList(SERIES_6, new Series[]{SERIES_7,SERIES_8,SERIES_9});
    private static final SearchResponse<Series> EMPTY_SEARCH_RESPONSE = new SearchResponse<>();
    private static final SearchResponse<Series> SEARCH_RESPONSE_PAGE_1 = new SearchResponse<>();
    private static final SearchResponse<Series> SEARCH_RESPONSE_PAGE_2 = new SearchResponse<>();
    static {
        SEARCH_RESPONSE_PAGE_1.setSearch(SERIES_PAGE_1);
        SEARCH_RESPONSE_PAGE_1.setTotalResults(9);
        SEARCH_RESPONSE_PAGE_2.setSearch(SERIES_PAGE_2);
        SEARCH_RESPONSE_PAGE_2.setTotalResults(9);
    }
    private static final Throwable ERROR = new Throwable();

    private SeriesDataProviderImpl underTest;
    @Mock
    private OmdbService omdbServiceMock;

    @Before
    public void setUp() throws Exception {
        underTest = new SeriesDataProviderImpl(omdbServiceMock);
    }

    @Test
    public void searchSeriesWhenNotCompleted() throws Exception {
        when(omdbServiceMock.searchSeries(KEYWORD, 1)).thenReturn(Observable.just(SEARCH_RESPONSE_PAGE_1));
        TestSubscriber<List<Series>> testSubscriber = new TestSubscriber<>();
        underTest.searchSeries(KEYWORD).subscribe(testSubscriber);
        testSubscriber.assertValue(SERIES_PAGE_1);
        testSubscriber.assertNotCompleted();
    }

    @Test
    public void searchSeriesWhenCompleted() throws Exception {
        when(omdbServiceMock.searchSeries(KEYWORD, 1)).thenReturn(Observable.just(SEARCH_RESPONSE_PAGE_1));
        when(omdbServiceMock.searchSeries(KEYWORD, 2)).thenReturn(Observable.just(SEARCH_RESPONSE_PAGE_2));
        TestSubscriber<List<Series>> testSubscriber = new TestSubscriber<>();
        underTest.searchSeries(KEYWORD).subscribe(testSubscriber);
        underTest.loadPage();
        RxJavaTestRunner.getTestScheduler().triggerActions();
        testSubscriber.assertValues(SERIES_PAGE_1, SERIES_PAGE_2);
        testSubscriber.assertCompleted();
    }

    @Test
    public void searchSeriesWhenEmpty() throws Exception {
        when(omdbServiceMock.searchSeries(KEYWORD, 1)).thenReturn(Observable.just(EMPTY_SEARCH_RESPONSE));
        TestSubscriber<List<Series>> testSubscriber = new TestSubscriber<>();
        underTest.searchSeries(KEYWORD).subscribe(testSubscriber);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        testSubscriber.assertValue(Collections.emptyList());
        testSubscriber.assertCompleted();
    }

    @Test
    public void searchSeriesWhenError() throws Exception {
        when(omdbServiceMock.searchSeries(KEYWORD, 1)).thenReturn(Observable.error(ERROR));
        TestSubscriber<List<Series>> testSubscriber = new TestSubscriber<>();
        underTest.searchSeries(KEYWORD).subscribe(testSubscriber);
        testSubscriber.assertError(ERROR);
        testSubscriber.assertNotCompleted();
        assertEquals(0, underTest.page);
    }

    @Test
    public void searchSeriesWhenSecondPageError() throws Exception {
        when(omdbServiceMock.searchSeries(KEYWORD, 1)).thenReturn(Observable.just(SEARCH_RESPONSE_PAGE_1));
        when(omdbServiceMock.searchSeries(KEYWORD, 2)).thenReturn(Observable.error(ERROR));
        TestSubscriber<List<Series>> testSubscriber = new TestSubscriber<>();
        underTest.searchSeries(KEYWORD).subscribe(testSubscriber);
        underTest.loadPage();
        RxJavaTestRunner.getTestScheduler().triggerActions();
        testSubscriber.assertValue(SERIES_PAGE_1);
        testSubscriber.assertError(ERROR);
        testSubscriber.assertNotCompleted();
        assertEquals(1, underTest.page);
    }

    @Test
    public void getSeriesByIdWhenSuccess() throws Exception {
        when(omdbServiceMock.getSeriesById(IMDB_ID)).thenReturn(Observable.just(SERIES_1));
        TestSubscriber<Series> testSubscriber = new TestSubscriber<>();
        underTest.getSeriesById(IMDB_ID).subscribe(testSubscriber);
        testSubscriber.assertValue(SERIES_1);
        testSubscriber.assertCompleted();
    }

    @Test
    public void getSeriesByIdWhenError() throws Exception {
        when(omdbServiceMock.getSeriesById(IMDB_ID)).thenReturn(Observable.error(ERROR));
        TestSubscriber<Series> testSubscriber = new TestSubscriber<>();
        underTest.getSeriesById(IMDB_ID).subscribe(testSubscriber);
        testSubscriber.assertError(ERROR);
        testSubscriber.assertNotCompleted();
    }
}