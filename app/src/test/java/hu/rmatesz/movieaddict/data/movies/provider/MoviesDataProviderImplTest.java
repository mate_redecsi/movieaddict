package hu.rmatesz.movieaddict.data.movies.provider;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;

import hu.rmatesz.movieaddict.RxJavaTestRunner;
import hu.rmatesz.movieaddict.api.model.Movie;
import hu.rmatesz.movieaddict.api.service.OmdbService;
import hu.rmatesz.movieaddict.api.service.response.SearchResponse;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by Mate_Redecsi on 12/29/2016.
 */
@RunWith(RxJavaTestRunner.class)
public class MoviesDataProviderImplTest {
    private static final String KEYWORD = "keyword";
    private static final String IMDB_ID = "imdbID";
    private static final Movie MOVIE_1 = new Movie();
    private static final Movie MOVIE_2 = new Movie();
    private static final Movie MOVIE_3 = new Movie();
    private static final Movie MOVIE_4 = new Movie();
    private static final Movie MOVIE_5 = new Movie();
    private static final Movie MOVIE_6 = new Movie();
    private static final Movie MOVIE_7 = new Movie();
    private static final Movie MOVIE_8 = new Movie();
    private static final Movie MOVIE_9 = new Movie();
    private static final List<Movie> MOVIES_PAGE_1 = Lists.asList(MOVIE_1, new Movie[]{MOVIE_2,MOVIE_3,MOVIE_4,MOVIE_5});
    private static final List<Movie> MOVIES_PAGE_2 = Lists.asList(MOVIE_6, new Movie[]{MOVIE_7,MOVIE_8,MOVIE_9});
    private static final SearchResponse<Movie> EMPTY_SEARCH_RESPONSE = new SearchResponse<>();
    private static final SearchResponse<Movie> SEARCH_RESPONSE_PAGE_1 = new SearchResponse<>();
    private static final SearchResponse<Movie> SEARCH_RESPONSE_PAGE_2 = new SearchResponse<>();
    static {
        SEARCH_RESPONSE_PAGE_1.setSearch(MOVIES_PAGE_1);
        SEARCH_RESPONSE_PAGE_1.setTotalResults(9);
        SEARCH_RESPONSE_PAGE_2.setSearch(MOVIES_PAGE_2);
        SEARCH_RESPONSE_PAGE_2.setTotalResults(9);
    }
    private static final Throwable ERROR = new Throwable();

    private MoviesDataProviderImpl underTest;
    @Mock
    private OmdbService omdbServiceMock;

    @Before
    public void setUp() throws Exception {
        underTest = new MoviesDataProviderImpl(omdbServiceMock);
    }

    @Test
    public void searchMoviesWhenNotCompleted() throws Exception {
        when(omdbServiceMock.searchMovies(KEYWORD, 1)).thenReturn(Observable.just(SEARCH_RESPONSE_PAGE_1));
        TestSubscriber<List<Movie>> testSubscriber = new TestSubscriber<>();
        underTest.searchMovies(KEYWORD).subscribe(testSubscriber);
        testSubscriber.assertValue(MOVIES_PAGE_1);
        testSubscriber.assertNotCompleted();
    }

    @Test
    public void searchMoviesWhenCompleted() throws Exception {
        when(omdbServiceMock.searchMovies(KEYWORD, 1)).thenReturn(Observable.just(SEARCH_RESPONSE_PAGE_1));
        when(omdbServiceMock.searchMovies(KEYWORD, 2)).thenReturn(Observable.just(SEARCH_RESPONSE_PAGE_2));
        TestSubscriber<List<Movie>> testSubscriber = new TestSubscriber<>();
        underTest.searchMovies(KEYWORD).subscribe(testSubscriber);
        underTest.loadPage();
        RxJavaTestRunner.getTestScheduler().triggerActions();
        testSubscriber.assertValues(MOVIES_PAGE_1, MOVIES_PAGE_2);
        testSubscriber.assertCompleted();
    }

    @Test
    public void searchMoviesWhenEmpty() throws Exception {
        when(omdbServiceMock.searchMovies(KEYWORD, 1)).thenReturn(Observable.just(EMPTY_SEARCH_RESPONSE));
        TestSubscriber<List<Movie>> testSubscriber = new TestSubscriber<>();
        underTest.searchMovies(KEYWORD).subscribe(testSubscriber);
        RxJavaTestRunner.getTestScheduler().triggerActions();
        testSubscriber.assertValue(Collections.emptyList());
        testSubscriber.assertCompleted();
    }

    @Test
    public void searchMoviesWhenError() throws Exception {
        when(omdbServiceMock.searchMovies(KEYWORD, 1)).thenReturn(Observable.error(ERROR));
        TestSubscriber<List<Movie>> testSubscriber = new TestSubscriber<>();
        underTest.searchMovies(KEYWORD).subscribe(testSubscriber);
        testSubscriber.assertError(ERROR);
        testSubscriber.assertNotCompleted();
        assertEquals(0, underTest.page);
    }

    @Test
    public void searchMoviesWhenSecondPageError() throws Exception {
        when(omdbServiceMock.searchMovies(KEYWORD, 1)).thenReturn(Observable.just(SEARCH_RESPONSE_PAGE_1));
        when(omdbServiceMock.searchMovies(KEYWORD, 2)).thenReturn(Observable.error(ERROR));
        TestSubscriber<List<Movie>> testSubscriber = new TestSubscriber<>();
        underTest.searchMovies(KEYWORD).subscribe(testSubscriber);
        underTest.loadPage();
        RxJavaTestRunner.getTestScheduler().triggerActions();
        testSubscriber.assertValue(MOVIES_PAGE_1);
        testSubscriber.assertError(ERROR);
        testSubscriber.assertNotCompleted();
        assertEquals(1, underTest.page);
    }

    @Test
    public void getMovieByIdWhenSuccess() throws Exception {
        when(omdbServiceMock.getMovieById(IMDB_ID)).thenReturn(Observable.just(MOVIE_1));
        TestSubscriber<Movie> testSubscriber = new TestSubscriber<>();
        underTest.getMovieById(IMDB_ID).subscribe(testSubscriber);
        testSubscriber.assertValue(MOVIE_1);
        testSubscriber.assertCompleted();
    }

    @Test
    public void getMovieByIdWhenError() throws Exception {
        when(omdbServiceMock.getMovieById(IMDB_ID)).thenReturn(Observable.error(ERROR));
        TestSubscriber<Movie> testSubscriber = new TestSubscriber<>();
        underTest.getMovieById(IMDB_ID).subscribe(testSubscriber);
        testSubscriber.assertError(ERROR);
        testSubscriber.assertNotCompleted();
    }

}