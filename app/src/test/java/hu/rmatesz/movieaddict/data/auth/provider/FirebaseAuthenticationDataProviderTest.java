package hu.rmatesz.movieaddict.data.auth.provider;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import hu.rmatesz.movieaddict.data.auth.model.User;
import hu.rmatesz.movieaddict.firebase.FirebaseAuthRxDecorator;
import rx.Observable;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Mate_Redecsi on 12/29/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class FirebaseAuthenticationDataProviderTest {
    private final static String EMAIL = "email";
    private final static String PASSWORD = "password";
    private FirebaseAuthenticationDataProvider underTest;
    @Mock
    private FirebaseAuthRxDecorator firebaseAuthMock;
    @Mock
    private FirebaseUser firebaseUserMock;
    @Mock
    private AuthResult authResultMock;

    @Before
    public void setUp() throws Exception {
        underTest = new FirebaseAuthenticationDataProvider(firebaseAuthMock);
    }

    @Test
    public void signIn() throws Exception {
        when(firebaseAuthMock.signInWithEmailAndPassword(EMAIL, PASSWORD)).thenReturn(Observable.just(authResultMock));
        when(authResultMock.getUser()).thenReturn(firebaseUserMock);
        when(firebaseUserMock.getEmail()).thenReturn(EMAIL);
        TestSubscriber<User> testSubscriber = new TestSubscriber<>();
        underTest.signIn(EMAIL, PASSWORD).subscribeOn(Schedulers.immediate()).observeOn(Schedulers.immediate()).subscribe(testSubscriber);
        testSubscriber.assertValue(new User(EMAIL, null));
    }

    @Test
    public void signUp() throws Exception {
        when(firebaseAuthMock.createUserWithEmailAndPassword(EMAIL, PASSWORD)).thenReturn(Observable.just(authResultMock));
        when(authResultMock.getUser()).thenReturn(firebaseUserMock);
        when(firebaseUserMock.getEmail()).thenReturn(EMAIL);
        TestSubscriber<User> testSubscriber = new TestSubscriber<>();
        underTest.signUp(EMAIL, PASSWORD).subscribeOn(Schedulers.immediate()).observeOn(Schedulers.immediate()).subscribe(testSubscriber);
        testSubscriber.assertValue(new User(EMAIL, null));
    }

    @Test
    public void signOut() throws Exception {
        underTest.signOut();
        verify(firebaseAuthMock).signOut();
    }

}