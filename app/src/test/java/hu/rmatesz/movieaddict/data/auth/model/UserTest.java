package hu.rmatesz.movieaddict.data.auth.model;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Mate_Redecsi on 1/1/2017.
 */
public class UserTest {
    private static final User USER_1 = new User("test", "test");
    private static final User USER_2 = new User("test", "test1");
    private static final User USER_3 = new User("test1", "test");
    private static final User USER_4 = new User("test", null);
    private static final User USER_5 = new User("test", "");
    private static final User USER_6 = new User("test", null);
    private static final User USER_7 = new User("test", "test");

    @Test
    public void equalsWhenTrue() throws Exception {
        boolean equals = USER_1.equals(USER_7);
        assertTrue(equals);
    }

    @Test
    public void equalsWhenDifferentEmail() throws Exception {
        boolean equals = USER_1.equals(USER_3);
        assertFalse(equals);
    }

    @Test
    public void equalsWhenDifferentPassword() throws Exception {
        boolean equals = USER_1.equals(USER_2);
        assertFalse(equals);
    }

    @Test
    public void equalsWhenTrueWithNullPassword() throws Exception {
        boolean equals = USER_4.equals(USER_6);
        assertTrue(equals);
    }

    @Test
    public void equalsWhenEmptyAndNullPassword() throws Exception {
        boolean equals = USER_4.equals(USER_5);
        assertFalse(equals);
    }

}