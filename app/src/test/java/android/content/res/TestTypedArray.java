package android.content.res;

/**
 * Created by Mate_Redecsi on 12/29/2016.
 */

public class TestTypedArray extends TypedArray {
    private final int[] resourceIds;

    public TestTypedArray(int[] resourceIds) {
        super();
        this.resourceIds = resourceIds;
    }

    @Override
    public int getResourceId(int index, int defValue) {
        return index < resourceIds.length ? resourceIds[index] : defValue;
    }
}
