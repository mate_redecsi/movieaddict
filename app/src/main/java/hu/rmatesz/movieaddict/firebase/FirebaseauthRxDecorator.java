package hu.rmatesz.movieaddict.firebase;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import rx.Observable;

/**
 * Created by Mate_Redecsi on 12/29/2016.
 */

public class FirebaseAuthRxDecorator {
    private final FirebaseAuth firebaseAuth;
    public FirebaseAuthRxDecorator(FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    public Observable<AuthResult> signInWithEmailAndPassword(String email, String password) {
        return Observable.create(subscriber ->
                firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnSuccessListener(subscriber::onNext)
                .addOnFailureListener(subscriber::onError)
                .addOnCompleteListener(result -> subscriber.onCompleted()));
    }

    public Observable<AuthResult> createUserWithEmailAndPassword(String email, String password) {
        return Observable.create(subscriber ->
                firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnSuccessListener(subscriber::onNext)
                .addOnFailureListener(subscriber::onError)
                .addOnCompleteListener(authResult -> subscriber.onCompleted()));
    }

    public void signOut() {
        firebaseAuth.signOut();
    }
}
