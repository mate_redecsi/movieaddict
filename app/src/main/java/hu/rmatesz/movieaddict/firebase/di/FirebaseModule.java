package hu.rmatesz.movieaddict.firebase.di;

import com.google.firebase.auth.FirebaseAuth;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.firebase.FirebaseAuthRxDecorator;

/**
 * Created by Mate Redecsi on 2016. 10. 26..
 */
@Module
public class FirebaseModule {
    @Provides
    FirebaseAuth provideFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }

    @Provides
    FirebaseAuthRxDecorator provideFirebaseAuthRxDecorator(FirebaseAuth firebaseAuth) {
        return new FirebaseAuthRxDecorator(firebaseAuth);
    }
}
