package hu.rmatesz.movieaddict.application;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import hu.rmatesz.movieaddict.common.di.ApplicationComponent;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */

public class MovieAddictApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationComponent.Injector.inject(this);

        Fresco.initialize(this);
    }
}
