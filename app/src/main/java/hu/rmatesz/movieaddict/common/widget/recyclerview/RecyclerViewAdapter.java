package hu.rmatesz.movieaddict.common.widget.recyclerview;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import hu.rmatesz.movieaddict.BR;

/**
 * Created by Mate_Redecsi on 10/20/2016.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private List<ListItemViewModel> items;
    private List<Integer> viewTypes;
    private List<Integer> itemLayouts;
    private int loadingLayout;
    private boolean showLoading;

    public RecyclerViewAdapter() {
    }

    @Override
    public BindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int itemLayout = viewType == itemLayouts.size() ? loadingLayout : itemLayouts.get(viewTypes != null ? viewTypes.indexOf(viewType) : viewType);
        ViewDataBinding dataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), itemLayout, parent, false);
        return new BindingViewHolder(dataBinding);
    }

    @Override
    public void onBindViewHolder(BindingViewHolder holder, int position) {
        if (position < items.size()) {
            holder.getBinding().setVariable(BR.viewModel, items.get(position));
            holder.getBinding().executePendingBindings();
        }
    }

    @Override
    public int getItemCount() {
        return (items == null ? 0 : items.size()) + (showLoading ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return position == items.size() ? itemLayouts.size() : items.get(position).getViewType();
    }

    public void setItems(List<ListItemViewModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setItemLayout(int itemLayout) {
        this.itemLayouts = Collections.singletonList(itemLayout);
    }

    public void setItemLayouts(List<Integer> itemLayouts) {
        this.itemLayouts = itemLayouts;
    }

    public void setShowLoading(boolean showLoading) {
        this.showLoading = showLoading;
        notifyDataSetChanged();
    }

    public void setLoadingLayout(int loadingLayout) {
        this.loadingLayout = loadingLayout;
    }

    public void setViewTypes(List<Integer> viewTypes) {
        this.viewTypes = viewTypes;
    }
}
