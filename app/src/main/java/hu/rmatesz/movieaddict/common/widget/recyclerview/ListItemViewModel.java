package hu.rmatesz.movieaddict.common.widget.recyclerview;

import android.databinding.BaseObservable;

/**
 * Created by Mate_Redecsi on 10/21/2016.
 */
public abstract class ListItemViewModel extends BaseObservable {
    public abstract int getViewType();
}
