package hu.rmatesz.movieaddict.common.widget.swiperefresh;

import android.support.v4.widget.SwipeRefreshLayout;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */

public class SwipeRefreshLayoutBindingAdapterImpl implements SwipeRefreshLayoutBindingAdapter {
    @Override
    public void setOnRefreshListener(SwipeRefreshLayout swipeRefreshLayout, SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
    }
}
