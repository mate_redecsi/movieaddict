package hu.rmatesz.movieaddict.common.widget.draweeview;

import android.databinding.BindingAdapter;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */

public interface DraweeViewBindingAdapter {
    @BindingAdapter("imageUrl")
    void loadImageUrl(SimpleDraweeView materialDraweeView, String imageUrl);

    @BindingAdapter({"imageUrl", "loadPalette"})
    void loadImageUrl(SimpleDraweeView materialDraweeView, String imageUrl, int palette);
}
