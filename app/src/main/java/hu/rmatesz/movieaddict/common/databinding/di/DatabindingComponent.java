package hu.rmatesz.movieaddict.common.databinding.di;

import android.databinding.DataBindingComponent;

import dagger.Subcomponent;

/**
 * Created by Mate_Redecsi on 11/3/2016.
 */
@Subcomponent(modules = { DatabindingModule.class })
public interface DatabindingComponent {
    DataBindingComponent dataBindingComponenet();

    @Subcomponent.Builder
    interface Builder {
        DatabindingComponent build();
    }
}
