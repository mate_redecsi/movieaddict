package hu.rmatesz.movieaddict.common.widget.swiperefresh;

import android.databinding.BindingAdapter;
import android.support.v4.widget.SwipeRefreshLayout;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */

public interface SwipeRefreshLayoutBindingAdapter {
    @BindingAdapter("onRefresh")
    void setOnRefreshListener(SwipeRefreshLayout swipeRefreshLayout, SwipeRefreshLayout.OnRefreshListener onRefreshListener);
}
