package hu.rmatesz.movieaddict.common.widget.viewpager;

import android.databinding.Observable;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */

public interface ViewPagerItemViewModel extends Observable {
    String getTitle();
}
