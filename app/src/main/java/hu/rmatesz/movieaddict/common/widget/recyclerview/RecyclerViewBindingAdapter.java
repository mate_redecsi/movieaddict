package hu.rmatesz.movieaddict.common.widget.recyclerview;

import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by Mate_Redecsi on 10/19/2016.
 */
public interface RecyclerViewBindingAdapter {
    @BindingAdapter("columns")
    void setupRecyclerViewColumns(RecyclerView recyclerView, int columns);

    @BindingAdapter("itemSource")
    void setupRecyclerViewSource(RecyclerView recyclerView, List<ListItemViewModel> source);

    @BindingAdapter("itemLayout")
    void setupRecyclerViewSource(RecyclerView recyclerView, int itemLayout);

    @BindingAdapter("itemLayouts")
    void setupRecyclerItemLayouts(RecyclerView recyclerView, TypedArray itemLayouts);

    @BindingAdapter("loadingLayout")
    void setupRecyclerViewLoadingLayout(RecyclerView recyclerView, int loadingLayout);

    @BindingAdapter("showLoading")
    void setupRecyclerViewShowLoading(RecyclerView recyclerView, boolean showLoading);

    @BindingAdapter("onLastElementReached")
    void setOnLastElementReachedListener(RecyclerView recyclerView, OnLastElementReachedListener listener);

}
