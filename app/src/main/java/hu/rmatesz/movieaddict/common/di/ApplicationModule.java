package hu.rmatesz.movieaddict.common.di;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mate_Redecsi on 10/21/2016.
 */
@Module
public class ApplicationModule {
    private final Application application;
    ApplicationModule(Application application) {
        this.application = application;
    }

    @ApplicationContext
    @Singleton
    @Provides
    Context provideApplicationContext() {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    Resources provideResources(@ApplicationContext Context context) {
        return context.getResources();
    }

}
