package hu.rmatesz.movieaddict.common.widget.toolbar;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */

public class ToolbarBindingAdapterImpl implements ToolbarBindingAdapter {
    private final AppCompatActivity activity;

    public ToolbarBindingAdapterImpl(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void setToolbarAsSupportActionBar(Toolbar toolbar, boolean asSupportActionBar) {
        if (asSupportActionBar) {
            activity.setSupportActionBar(toolbar);
        }
    }

    @Override
    public void setHomeAsUpEnabled(Toolbar toolbar, boolean asSupportActionBar, boolean displayHomeAsUpEnabled) {
        setToolbarAsSupportActionBar(toolbar, asSupportActionBar);
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(displayHomeAsUpEnabled);
        }
    }
}
