package hu.rmatesz.movieaddict.common.widget.drawer;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.MenuRes;
import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;

import com.android.databinding.library.baseAdapters.BR;

/**
 * Created by Mate Redecsi on 2016. 10. 27..
 */

public class NavigationViewBindingAdapterImpl implements NavigationViewBindingAdapter {
    @Override
    public void setOnNavigationItemSelectedListener(NavigationView navigationView, NavigationView.OnNavigationItemSelectedListener listener) {
        navigationView.setNavigationItemSelectedListener(listener);
    }

    @Override
    public void setCheckedItem(NavigationView navigationView, int checkedItem) {
        navigationView.setCheckedItem(checkedItem);
    }

    @Override
    public void setHeaderLayout(NavigationView navigationView, @LayoutRes int headerLayout) {
        cleanHeader(navigationView);
        navigationView.inflateHeaderView(headerLayout);
    }

    @Override
    public void setHeaderLayout(NavigationView navigationView, @LayoutRes int headerLayout, Observable viewModel) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(navigationView.getContext()), headerLayout, navigationView, false);
        viewDataBinding.setVariable(BR.viewModel, viewModel);
        cleanHeader(navigationView);
        navigationView.addHeaderView(viewDataBinding.getRoot());
    }

    @Override
    public void setMenu(NavigationView navigationView, @MenuRes int menu) {
        navigationView.getMenu().clear();
        navigationView.inflateMenu(menu);
    }

    private void cleanHeader(NavigationView navigationView) {
        while (navigationView.getHeaderCount() > 0) {
            navigationView.removeHeaderView(navigationView.getHeaderView(0));
        }
    }
}
