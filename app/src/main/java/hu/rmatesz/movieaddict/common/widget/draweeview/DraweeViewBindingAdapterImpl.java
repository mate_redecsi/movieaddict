package hu.rmatesz.movieaddict.common.widget.draweeview;

import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.graphics.Palette;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */

public class DraweeViewBindingAdapterImpl implements DraweeViewBindingAdapter {

    private final ViewDataBinding viewDataBinding;

    public DraweeViewBindingAdapterImpl(ViewDataBinding viewDataBinding) {
        this.viewDataBinding = viewDataBinding;
    }

    @Override
    public void loadImageUrl(SimpleDraweeView materialDraweeView, String imageUrl) {
        if (imageUrl != null) {
            materialDraweeView.setImageURI(Uri.parse(imageUrl));
        }
    }

    @Override
    public void loadImageUrl(SimpleDraweeView materialDraweeView, String imageUrl, int paletteVariable) {
        loadImageUrl(materialDraweeView, imageUrl);
        Bitmap bmp = null;
        viewDataBinding.setVariable(paletteVariable, Palette.from(bmp));
    }
}
