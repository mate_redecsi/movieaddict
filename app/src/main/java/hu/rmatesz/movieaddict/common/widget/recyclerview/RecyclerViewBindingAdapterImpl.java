package hu.rmatesz.movieaddict.common.widget.recyclerview;

import android.content.res.TypedArray;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import java.util.List;

import hu.rmatesz.movieaddict.util.ResourcesUtil;

/**
 * Created by Mate_Redecsi on 10/19/2016.
 */
public final class RecyclerViewBindingAdapterImpl implements RecyclerViewBindingAdapter {
    private final ResourcesUtil resourcesUtil;

    public RecyclerViewBindingAdapterImpl(ResourcesUtil resourcesUtil) {
        this.resourcesUtil = resourcesUtil;
    }

    @Override
    public void setupRecyclerViewColumns(RecyclerView recyclerView, int columns) {
        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), columns));
    }

    @Override
    public void setupRecyclerViewSource(RecyclerView recyclerView, List<ListItemViewModel> source) {
        getAdapter(recyclerView).setItems(source);
    }

    @Override
    public void setupRecyclerViewSource(RecyclerView recyclerView, int itemLayout) {
        getAdapter(recyclerView).setItemLayout(itemLayout);
    }

    @Override
    public void setupRecyclerItemLayouts(RecyclerView recyclerView, TypedArray itemLayouts) {
        getAdapter(recyclerView).setItemLayouts(resourcesUtil.getResourceList(itemLayouts));
    }

    @Override
    public void setupRecyclerViewLoadingLayout(RecyclerView recyclerView, int loadingLayout) {
        getAdapter(recyclerView).setLoadingLayout(loadingLayout);
    }

    @Override
    public void setupRecyclerViewShowLoading(RecyclerView recyclerView, boolean showLoading) {
        getAdapter(recyclerView).setShowLoading(showLoading);
    }

    @Override
    public void setOnLastElementReachedListener(RecyclerView recyclerView, OnLastElementReachedListener listener) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (endReached(recyclerView)) {
                    listener.onLastElementReached();
            }
            }
        });
    }

    private RecyclerViewAdapter getAdapter(RecyclerView recyclerView) {
        RecyclerViewAdapter adapter = (RecyclerViewAdapter) recyclerView.getAdapter();
        if (adapter == null) {
            adapter = new RecyclerViewAdapter();
            recyclerView.setAdapter(adapter);
        }
        return adapter;
    }

    private boolean endReached(RecyclerView recyclerView) {
        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = recyclerView.getLayoutManager().getItemCount();

        int firstVisibleItemPosition;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        } else if (recyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {
            // https://code.google.com/p/android/issues/detail?id=181461
            if (recyclerView.getLayoutManager().getChildCount() > 0) {
                firstVisibleItemPosition = ((StaggeredGridLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPositions(null)[0];
            } else {
                firstVisibleItemPosition = 0;
            }
        } else {
            throw new IllegalStateException("LayoutManager needs to subclass LinearLayoutManager or StaggeredGridLayoutManager");
        }

        // Check if end of the list is reached (counting threshold) or if there is no items at all
        return (totalItemCount - visibleItemCount) <= (firstVisibleItemPosition + 1);
    }
}
