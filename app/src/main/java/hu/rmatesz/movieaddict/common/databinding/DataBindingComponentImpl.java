package hu.rmatesz.movieaddict.common.databinding;

import hu.rmatesz.movieaddict.common.widget.draweeview.DraweeViewBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.drawer.DrawerLayoutBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.drawer.NavigationViewBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.recyclerview.RecyclerViewBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.swiperefresh.SwipeRefreshLayoutBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.toolbar.ToolbarBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.viewpager.ViewPagerBindingAdapter;

/**
 * Created by Mate_Redecsi on 10/27/2016.
 */

public class DataBindingComponentImpl implements android.databinding.DataBindingComponent {
    private final SwipeRefreshLayoutBindingAdapter swipeRefreshLayoutBindingAdapter;
    private final RecyclerViewBindingAdapter recyclerViewBindingAdapter;
    private final DraweeViewBindingAdapter draweeViewBindingAdapter;
    private final NavigationViewBindingAdapter navigationViewBindingAdapter;
    private final DrawerLayoutBindingAdapter drawerLayoutBindingAdapter;
    private final ViewPagerBindingAdapter viewPagerBindingAdapter;
    private final ToolbarBindingAdapter toolbarBindingAdapter;

    public DataBindingComponentImpl(SwipeRefreshLayoutBindingAdapter swipeRefreshLayoutBindingAdapter, RecyclerViewBindingAdapter recyclerViewBindingAdapter,
                                    DraweeViewBindingAdapter draweeViewBindingAdapter, NavigationViewBindingAdapter navigationViewBindingAdapter,
                                    DrawerLayoutBindingAdapter drawerLayoutBindingAdapter, ViewPagerBindingAdapter viewPagerBindingAdapter, ToolbarBindingAdapter toolbarBindingAdapter) {
        this.swipeRefreshLayoutBindingAdapter = swipeRefreshLayoutBindingAdapter;
        this.recyclerViewBindingAdapter = recyclerViewBindingAdapter;
        this.draweeViewBindingAdapter = draweeViewBindingAdapter;
        this.navigationViewBindingAdapter = navigationViewBindingAdapter;
        this.drawerLayoutBindingAdapter = drawerLayoutBindingAdapter;
        this.viewPagerBindingAdapter = viewPagerBindingAdapter;
        this.toolbarBindingAdapter = toolbarBindingAdapter;
    }

    @Override
    public SwipeRefreshLayoutBindingAdapter getSwipeRefreshLayoutBindingAdapter() {
        return swipeRefreshLayoutBindingAdapter;
    }

    @Override
    public RecyclerViewBindingAdapter getRecyclerViewBindingAdapter() {
        return recyclerViewBindingAdapter;
    }

    @Override
    public DraweeViewBindingAdapter getDraweeViewBindingAdapter() {
        return draweeViewBindingAdapter;
    }

    @Override
    public NavigationViewBindingAdapter getNavigationViewBindingAdapter() {
        return navigationViewBindingAdapter;
    }

    @Override
    public DrawerLayoutBindingAdapter getDrawerLayoutBindingAdapter() {
        return drawerLayoutBindingAdapter;
    }

    @Override
    public ViewPagerBindingAdapter getViewPagerBindingAdapter() {
        return viewPagerBindingAdapter;
    }

    @Override
    public ToolbarBindingAdapter getToolbarBindingAdapter() {
        return toolbarBindingAdapter;
    }
}
