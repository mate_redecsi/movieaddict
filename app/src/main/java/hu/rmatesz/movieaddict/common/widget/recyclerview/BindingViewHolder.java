package hu.rmatesz.movieaddict.common.widget.recyclerview;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Mate_Redecsi on 10/19/2016.
 */
public class BindingViewHolder extends RecyclerView.ViewHolder {
    private final ViewDataBinding binding;
    public BindingViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public ViewDataBinding getBinding() {
        return binding;
    }
}
