package hu.rmatesz.movieaddict.common.widget.drawer;

import android.support.annotation.StringRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import hu.rmatesz.movieaddict.presentation.drawer.listener.OnDrawerClosedListener;
import hu.rmatesz.movieaddict.presentation.drawer.listener.OnDrawerOpenedListener;

/**
 * Created by Mate Redecsi on 2016. 10. 27..
 */

public class DrawerLayoutBindingAdapterImpl implements DrawerLayoutBindingAdapter {
    @Override
    public void setupDrawerToggle(DrawerLayout drawerLayout,
                                         Toolbar toolbar, @StringRes int openDrawerText, @StringRes int closeDrawerText) {
        setupDrawerToggle(drawerLayout, null, null, toolbar, openDrawerText, closeDrawerText);
    }

    @Override
    public void setupDrawerToggle(DrawerLayout drawerLayout, OnDrawerOpenedListener onDrawerOpenedListener, OnDrawerClosedListener onDrawerClosedListener,
                                         Toolbar toolbar, @StringRes int openDrawerText, @StringRes int closeDrawerText) {
        AppCompatActivity activity = (AppCompatActivity) drawerLayout.getContext();
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(activity, drawerLayout, toolbar, openDrawerText, closeDrawerText) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (onDrawerClosedListener != null) {
                    onDrawerClosedListener.onDrawerClosed();
                }
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (onDrawerOpenedListener != null) {
                    onDrawerOpenedListener.onDrawerOpened();
                }
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);

        // Set the drawer toggle as the DrawerListener
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }
}
