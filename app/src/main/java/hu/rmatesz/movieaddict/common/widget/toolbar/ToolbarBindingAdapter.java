package hu.rmatesz.movieaddict.common.widget.toolbar;

import android.databinding.BindingAdapter;
import android.support.v7.widget.Toolbar;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */

public interface ToolbarBindingAdapter {
    @BindingAdapter("asSupportActionBar")
    void setToolbarAsSupportActionBar(Toolbar toolbar, boolean asSupportActionBar);

    @BindingAdapter({"asSupportActionBar", "displayHomeAsUpEnabled"})
    void setHomeAsUpEnabled(Toolbar toolbar, boolean asSupportActionBar, boolean displayHomeAsUpEnabled);
}
