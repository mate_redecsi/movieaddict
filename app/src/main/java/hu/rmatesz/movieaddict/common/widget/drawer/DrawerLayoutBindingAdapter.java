package hu.rmatesz.movieaddict.common.widget.drawer;

import android.databinding.BindingAdapter;
import android.support.annotation.StringRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;

import hu.rmatesz.movieaddict.presentation.drawer.listener.OnDrawerClosedListener;
import hu.rmatesz.movieaddict.presentation.drawer.listener.OnDrawerOpenedListener;

/**
 * Created by Mate Redecsi on 2016. 10. 27..
 */

public interface DrawerLayoutBindingAdapter {
    @BindingAdapter({"toolbar", "openDrawerText", "closeDrawerText"})
    void setupDrawerToggle(DrawerLayout drawerLayout, Toolbar toolbar, @StringRes int openDrawerText, @StringRes int closeDrawerText);

    @BindingAdapter({"onDrawerOpened", "onDrawerClosed", "toolbar", "openDrawerText", "closeDrawerText"})
    void setupDrawerToggle(DrawerLayout drawerLayout, OnDrawerOpenedListener onDrawerOpenedListener, OnDrawerClosedListener onDrawerClosedListener,
            Toolbar toolbar, @StringRes int openDrawerText, @StringRes int closeDrawerText);
}
