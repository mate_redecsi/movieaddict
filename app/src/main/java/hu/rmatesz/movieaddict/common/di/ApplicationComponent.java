package hu.rmatesz.movieaddict.common.di;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Component;
import hu.rmatesz.movieaddict.data.auth.di.AuthenticationDataComponent;
import hu.rmatesz.movieaddict.data.movies.di.MoviesDataComponent;
import hu.rmatesz.movieaddict.data.series.di.SeriesDataComponent;
import hu.rmatesz.movieaddict.util.EmailValidator;
import hu.rmatesz.movieaddict.util.PasswordValidator;
import hu.rmatesz.movieaddict.util.ResourcesUtil;
import hu.rmatesz.movieaddict.util.di.UtilsModule;

/**
 * Created by Mate_Redecsi on 10/21/2016.
 */
@Singleton
@Component(modules = { ApplicationModule.class, UtilsModule.class })
public interface ApplicationComponent {

    SeriesDataComponent.Builder seriesDataComponent();
    MoviesDataComponent.Builder moviesDataComponent();
    AuthenticationDataComponent.Builder authenticationDataComponent();

    @ApplicationContext
    Context context();

    Resources resources();

    ResourcesUtil resourcesUtil();
    EmailValidator emailValidator();
    PasswordValidator passwordValidator();

    final class Injector {
        private static ApplicationComponent component;
        public static void inject(Application application) {
            component = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(application))
                    .build();
        }

        public static ApplicationComponent getComponent() {
            return component;
        }
    }
}
