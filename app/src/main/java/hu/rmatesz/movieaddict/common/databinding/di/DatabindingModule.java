package hu.rmatesz.movieaddict.common.databinding.di;

import android.databinding.DataBindingComponent;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;

import javax.inject.Provider;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.common.widget.draweeview.DraweeViewBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.draweeview.DraweeViewBindingAdapterImpl;
import hu.rmatesz.movieaddict.common.widget.drawer.DrawerLayoutBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.drawer.DrawerLayoutBindingAdapterImpl;
import hu.rmatesz.movieaddict.common.widget.drawer.NavigationViewBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.drawer.NavigationViewBindingAdapterImpl;
import hu.rmatesz.movieaddict.common.widget.recyclerview.RecyclerViewBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.recyclerview.RecyclerViewBindingAdapterImpl;
import hu.rmatesz.movieaddict.common.widget.swiperefresh.SwipeRefreshLayoutBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.swiperefresh.SwipeRefreshLayoutBindingAdapterImpl;
import hu.rmatesz.movieaddict.common.databinding.DataBindingComponentImpl;
import hu.rmatesz.movieaddict.common.widget.toolbar.ToolbarBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.toolbar.ToolbarBindingAdapterImpl;
import hu.rmatesz.movieaddict.common.widget.viewpager.ViewPagerAdapter;
import hu.rmatesz.movieaddict.common.widget.viewpager.ViewPagerBindingAdapter;
import hu.rmatesz.movieaddict.common.widget.viewpager.ViewPagerBindingAdapterImpl;
import hu.rmatesz.movieaddict.util.ResourcesUtil;

/**
 * Created by Mate_Redecsi on 11/3/2016.
 */
@Module
public class DatabindingModule {
    @Provides
    DataBindingComponent provideDataBindingComponent(SwipeRefreshLayoutBindingAdapter swipeRefreshLayoutBindingAdapter, RecyclerViewBindingAdapter recyclerViewBindingAdapter,
                                                     DraweeViewBindingAdapter draweeViewBindingAdapter, NavigationViewBindingAdapter navigationViewBindingAdapter,
                                                     DrawerLayoutBindingAdapter drawerLayoutBindingAdapter, ViewPagerBindingAdapter viewPagerBindingAdapter,
                                                     ToolbarBindingAdapter toolbarBindingAdapter) {
        return new DataBindingComponentImpl(swipeRefreshLayoutBindingAdapter, recyclerViewBindingAdapter, draweeViewBindingAdapter,
                navigationViewBindingAdapter, drawerLayoutBindingAdapter, viewPagerBindingAdapter, toolbarBindingAdapter);
    }

    @Provides
    SwipeRefreshLayoutBindingAdapter provideSwipeRefreshLayoutBindingAdapter() {
        return new SwipeRefreshLayoutBindingAdapterImpl();
    }

    @Provides
    RecyclerViewBindingAdapter provideRecyclerViewBindingAdapter(ResourcesUtil resourcesUtil) {
        return new RecyclerViewBindingAdapterImpl(resourcesUtil);
    }

    @Provides
    DraweeViewBindingAdapter provideDraweeViewBindingAdapter() {
        return new DraweeViewBindingAdapterImpl();
    }

    @Provides
    NavigationViewBindingAdapter provideNavigationViewBindingAdapter() {
        return new NavigationViewBindingAdapterImpl();
    }

    @Provides
    DrawerLayoutBindingAdapter provideDrawerLayoutBindingAdapter() {
        return new DrawerLayoutBindingAdapterImpl();
    }

    @Provides
    ViewPagerBindingAdapter provideViewPagerBindingAdapter(Provider<ViewPagerAdapter> viewPagerAdapterProvider, ResourcesUtil resourcesUtil) {
        return new ViewPagerBindingAdapterImpl(viewPagerAdapterProvider, resourcesUtil);
    }

    @Provides
    ViewPagerAdapter provideViewPagerAdapter(LayoutInflater layoutInflater) {
        return new ViewPagerAdapter(layoutInflater);
    }

    @Provides
    ToolbarBindingAdapter provideToolbarBindingAdapter(AppCompatActivity activity) {
        return new ToolbarBindingAdapterImpl(activity);
    }
}
