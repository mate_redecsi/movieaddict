package hu.rmatesz.movieaddict.common.widget.drawer;

import android.databinding.BindingAdapter;
import android.databinding.Observable;
import android.support.annotation.LayoutRes;
import android.support.annotation.MenuRes;
import android.support.design.widget.NavigationView;

/**
 * Created by Mate Redecsi on 2016. 10. 27..
 */

public interface NavigationViewBindingAdapter {
    @BindingAdapter("onNavigationItemSelected")
    void setOnNavigationItemSelectedListener(NavigationView navigationView, NavigationView.OnNavigationItemSelectedListener listener);

    @BindingAdapter("checkedItem")
    void setCheckedItem(NavigationView navigationView, int checkedItem);

    @BindingAdapter("headerLayout")
    void setHeaderLayout(NavigationView navigationView, @LayoutRes int headerLayout);

    @BindingAdapter({"headerLayout", "headerViewModel"})
    void setHeaderLayout(NavigationView navigationView, @LayoutRes int headerLayout, Observable viewModel);

    @BindingAdapter("menu")
    void setMenu(NavigationView navigationView, @MenuRes int menu);
}
