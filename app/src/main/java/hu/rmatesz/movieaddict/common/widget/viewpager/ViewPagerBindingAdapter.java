package hu.rmatesz.movieaddict.common.widget.viewpager;

import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.support.v4.view.ViewPager;

import java.util.List;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */

public interface ViewPagerBindingAdapter {
    @BindingAdapter("itemSource")
    void setupViewPagerSource(ViewPager viewPager, List<ViewPagerItemViewModel> source);

    @BindingAdapter("itemLayouts")
    void setupViewPagerItemLayouts(ViewPager viewPager, TypedArray itemLayouts);
}
