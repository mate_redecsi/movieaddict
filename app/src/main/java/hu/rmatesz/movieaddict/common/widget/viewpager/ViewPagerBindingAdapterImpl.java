package hu.rmatesz.movieaddict.common.widget.viewpager;

import android.content.res.TypedArray;
import android.support.v4.view.ViewPager;

import java.util.List;

import javax.inject.Provider;

import hu.rmatesz.movieaddict.util.ResourcesUtil;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */

public class ViewPagerBindingAdapterImpl implements ViewPagerBindingAdapter {
    private final Provider<ViewPagerAdapter> viewPagerAdapterProvider;
    private final ResourcesUtil resourcesUtil;

    public ViewPagerBindingAdapterImpl(Provider<ViewPagerAdapter> viewPagerAdapterProvider, ResourcesUtil resourcesUtil) {
        this.viewPagerAdapterProvider = viewPagerAdapterProvider;
        this.resourcesUtil = resourcesUtil;
    }

    @Override
    public void setupViewPagerSource(ViewPager viewPager, List<ViewPagerItemViewModel> source) {
        getAdapter(viewPager).setItems(source);
    }

    @Override
    public void setupViewPagerItemLayouts(ViewPager viewPager, TypedArray itemLayouts) {
        getAdapter(viewPager).setItemLayouts(resourcesUtil.getResourceList(itemLayouts));
    }

    private ViewPagerAdapter getAdapter(ViewPager viewPager) {
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        if (adapter == null) {
            adapter = viewPagerAdapterProvider.get();
            viewPager.setAdapter(adapter);
        }
        return adapter;
    }
}
