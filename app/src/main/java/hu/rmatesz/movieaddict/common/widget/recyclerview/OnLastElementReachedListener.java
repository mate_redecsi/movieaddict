package hu.rmatesz.movieaddict.common.widget.recyclerview;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */

public interface OnLastElementReachedListener {
    void onLastElementReached();
}
