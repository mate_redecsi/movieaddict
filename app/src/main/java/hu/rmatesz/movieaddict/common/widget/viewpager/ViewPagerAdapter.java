package hu.rmatesz.movieaddict.common.widget.viewpager;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import hu.rmatesz.movieaddict.BR;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */

public class ViewPagerAdapter extends PagerAdapter {
    private final LayoutInflater layoutInflater;
    private List<ViewPagerItemViewModel> items;
    private List<Integer> itemLayouts;

    public ViewPagerAdapter(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(layoutInflater, itemLayouts.get(position), container, false);
        viewDataBinding.setVariable(BR.viewModel, items.get(position));
        container.addView(viewDataBinding.getRoot(), ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return viewDataBinding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return items.get(position).getTitle();
    }

    @Override
    public int getCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void setItems(List<ViewPagerItemViewModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setItemLayouts(List<Integer> itemLayouts) {
        this.itemLayouts = itemLayouts;
    }
}
