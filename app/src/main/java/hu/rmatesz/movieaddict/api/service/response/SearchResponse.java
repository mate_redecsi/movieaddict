package hu.rmatesz.movieaddict.api.service.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import hu.rmatesz.movieaddict.api.model.OmdbObject;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */

public class SearchResponse<T extends OmdbObject> {
    @SerializedName("Search")
    List<T> search;
    int totalResults;

    public List<T> getSearch() {
        return search;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setSearch(List<T> search) {
        this.search = search;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }
}
