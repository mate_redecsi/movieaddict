package hu.rmatesz.movieaddict.api.model;

/**
 * Created by Mate_Redecsi on 10/21/2016.
 */

public class Season {
    private String title;
    private int season;
    private int totalSeasons;
    private Episode[] episodes;
}
