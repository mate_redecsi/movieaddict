package hu.rmatesz.movieaddict.api.model;

/**
 * Created by Mate_Redecsi on 10/21/2016.
 */

public class Episode extends OmdbObject {
    private int episode;
    private int season;
    private String series;
}
