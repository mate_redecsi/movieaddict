package hu.rmatesz.movieaddict.api.service;

import hu.rmatesz.movieaddict.api.model.Episode;
import hu.rmatesz.movieaddict.api.model.Movie;
import hu.rmatesz.movieaddict.api.model.Season;
import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.api.service.response.SearchResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Mate_Redecsi on 10/21/2016.
 */
public interface OmdbService {
    @GET("?type=series&plot=full")
    Observable<Series> getSeriesByTitle(@Query("t") String title);

    @GET("?type=series&plot=full")
    Observable<Series> getSeriesById(@Query("i") String imdbId);

    @GET("?type=series")
    Observable<Season> getSeason(@Query("i") String seriesId, @Query("Season") int season);

    @GET("?type=series&plot=full")
    Observable<Episode> getEpisode(@Query("i") String imdbId);

    @GET("?type=series&plot=full")
    Observable<Season> getSeason(@Query("i") String seriesId, @Query("Season") int season, @Query("Episode") int episode);

    @GET("?type=series&plot=full")
    Observable<SearchResponse<Series>> searchSeries(@Query("s") String filter, @Query("page") int page);

    @GET("?type=movie&plot=full")
    Observable<Movie> getMovieById(@Query("i") String imdbId);

    @GET("?type=movie&plot=full")
    Observable<SearchResponse<Movie>> searchMovies(@Query("s") String filter, @Query("page") int page);
}
