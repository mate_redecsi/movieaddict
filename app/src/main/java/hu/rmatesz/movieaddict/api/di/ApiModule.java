package hu.rmatesz.movieaddict.api.di;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.api.service.OmdbService;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mate_Redecsi on 10/21/2016.
 */
@Module
public class ApiModule {
    @Provides
    @Named("omdbapi")
    public Retrofit provideOmdbRetrofitApi(Converter.Factory factory) {
        return new Retrofit.Builder()
                .baseUrl("http://www.omdbapi.com/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(factory)
                .build();
    }

    @Provides
    public OmdbService provideOmdbService(@Named("omdbapi") Retrofit retrofit) {
        return retrofit.create(OmdbService.class);
    }

    @Provides
    public Converter.Factory provideConverterFactory() {
        return GsonConverterFactory.create();
    }
}
