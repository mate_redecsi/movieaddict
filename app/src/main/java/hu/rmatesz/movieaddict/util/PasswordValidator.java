package hu.rmatesz.movieaddict.util;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */

public class PasswordValidator {
    private static final int PASSWORD_MIN_LENGTH = 6;

    public ValidationResult validatePassword(String password) {
        if (password == null || "".equals(password))
            return ValidationResult.EMPTY;
        else if (password.length() < PASSWORD_MIN_LENGTH)
            return ValidationResult.TOO_SHORT;
        else
            return ValidationResult.SUCCESS;
    }

    public enum ValidationResult {
        SUCCESS,
        EMPTY,
        TOO_SHORT
    }
}
