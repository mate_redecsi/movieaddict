package hu.rmatesz.movieaddict.util;

import android.content.res.TypedArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mate_Redecsi on 12/25/2016.
 */

public class ResourcesUtil {
    public List<Integer> getResourceList(TypedArray resourcesArray) {
        List<Integer> resourceList = new ArrayList<>();
        int item;
        int i = 0;
        do {
            item = resourcesArray.getResourceId(i, -1);
            if (item >= 0) {
                resourceList.add(item);
            }
            i++;
        } while (item >= 0);
        return resourceList;
    }
}
