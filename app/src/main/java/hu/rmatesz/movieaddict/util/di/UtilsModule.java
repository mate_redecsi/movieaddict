package hu.rmatesz.movieaddict.util.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.util.EmailValidator;
import hu.rmatesz.movieaddict.util.PasswordValidator;
import hu.rmatesz.movieaddict.util.ResourcesUtil;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */
@Module
public class UtilsModule {
    @Singleton
    @Provides
    ResourcesUtil provideResourcesUtil() {
        return new ResourcesUtil();
    }

    @Singleton
    @Provides
    EmailValidator provideEmailValidator() {
        return new EmailValidator();
    }

    @Singleton
    @Provides
    PasswordValidator providePasswordValidator() {
        return new PasswordValidator();
    }
}
