package hu.rmatesz.movieaddict.util;

import java.util.regex.Pattern;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */

public class EmailValidator {
    private static final Pattern EMAIL_FORMAT = Pattern
            .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+[A-Za-z0-9-]*(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    public ValidationResult validateEmail(String email) {
        if (email == null || "".equals(email))
            return ValidationResult.EMPTY;
        else if (!EMAIL_FORMAT.matcher(email).matches())
            return ValidationResult.INVALID_FORMAT;
        else
            return ValidationResult.SUCCESS;
    }

    public enum ValidationResult {
        SUCCESS,
        EMPTY,
        INVALID_FORMAT
    }
}
