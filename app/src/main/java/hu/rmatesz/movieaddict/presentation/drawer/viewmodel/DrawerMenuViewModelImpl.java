package hu.rmatesz.movieaddict.presentation.drawer.viewmodel;

import android.databinding.BaseObservable;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

import hu.rmatesz.movieaddict.BR;
import hu.rmatesz.movieaddict.R;
import hu.rmatesz.movieaddict.presentation.drawer.model.DrawerMenuModel;
import hu.rmatesz.movieaddict.presentation.drawer.router.DrawerMenuRouter;

/**
 * Created by Mate Redecsi on 2016. 10. 27..
 */

public class DrawerMenuViewModelImpl extends BaseObservable implements DrawerMenuViewModel, NavigationView.OnNavigationItemSelectedListener {
    private final DrawerMenuRouter router;
    private final DrawerMenuModel model;

    public DrawerMenuViewModelImpl(DrawerMenuRouter router, DrawerMenuModel model) {
        this.router = router;
        this.model = model;
    }

    @Override
    public void signInClicked(View v) {
        router.openSignIn();
    }

    @Override
    public void onStart() {
        notifyPropertyChanged(BR._all);
    }

    @Override
    public void onStop() {
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.sign_out:
                signOut();
                break;
        }
        return true;
    }

    private void signOut() {
        model.signOut();
        notifyPropertyChanged(BR._all);
    }

    @Override
    public boolean isAuthenticated() {
        return FirebaseAuth.getInstance().getCurrentUser() != null && !FirebaseAuth.getInstance().getCurrentUser().isAnonymous();
    }

    @Override
    public String getUserEmail() {
        return FirebaseAuth.getInstance().getCurrentUser().getEmail();
    }
}
