package hu.rmatesz.movieaddict.presentation.series.details.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import hu.rmatesz.movieaddict.presentation.series.details.model.SeriesDetailsModel;
import hu.rmatesz.movieaddict.presentation.series.details.router.SeriesDetailsRouter;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */

public class SeriesDetailsViewModelImpl extends BaseObservable implements SeriesDetailsViewModel {
    private final SeriesDetailsRouter router;
    private final SeriesDetailsModel model;
    @Bindable
    private String title;
    @Bindable
    private String posterUrl;

    public SeriesDetailsViewModelImpl(SeriesDetailsRouter router, SeriesDetailsModel model) {
        this.router = router;
        this.model = model;
        title = model.getSeries().getTitle();
        posterUrl = model.getSeries().getPoster();
        refresh();
    }

    @Override
    public void onRefresh() {
        refresh();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    private void refresh() {
        model.loadSeries();
//                title = series.getTitle();
//                posterUrl = series.getPoster();
//                notifyPropertyChanged(BR._all);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getPosterUrl() {
        return posterUrl;
    }

    @Override
    public boolean isFavourite() {
        return false;
    }
}
