package hu.rmatesz.movieaddict.presentation.auth.signin.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.text.Editable;
import android.view.View;

import hu.rmatesz.movieaddict.presentation.auth.signin.model.SignInModelImpl;

/**
 * Created by Mate Redecsi on 2016. 11. 03..
 */

public interface SignInViewModel extends SignInModelImpl.Callback, Observable {
    void onStart();
    void onStop();
    void onSignInClicked(View view);
    void onSignUpClicked(View view);

    void onEmailChanged(Editable s);

    void onPasswordChanged(Editable s);

    @Bindable
    String getEmail();

    @Bindable
    String getPassword();
}
