package hu.rmatesz.movieaddict.presentation.auth.signin.viewmodel;

import android.databinding.BaseObservable;
import android.text.Editable;
import android.view.View;

import hu.rmatesz.movieaddict.BR;
import hu.rmatesz.movieaddict.presentation.auth.signin.model.SignInModel;
import hu.rmatesz.movieaddict.presentation.auth.signin.router.SignInRouter;

/**
 * Created by Mate Redecsi on 2016. 10. 26..
 */

public class SignInViewModelImpl extends BaseObservable implements SignInViewModel {
    private final SignInRouter router;
    private final SignInModel model;
    String email;
    String password;

    public SignInViewModelImpl(SignInRouter router, SignInModel model) {
        this.router = router;
        this.model = model;
    }

    @Override
    public void onStart() {
        model.registerCallback(this);
    }

    @Override
    public void onStop() {
        model.unregisterCallback(this);
    }

    @Override
    public void onSignInClicked(View view) {
        model.signIn(email, password);
    }

    @Override
    public void onSignUpClicked(View view) {
        router.openSignUp();
    }

    @Override
    public void onEmailChanged(Editable s) {
        String newEmail = s.toString();
        if (!newEmail.equals(email)) {
            setEmail(newEmail);
        }
    }

    @Override
    public void onPasswordChanged(Editable s) {
        String newPassword = s.toString();
        if (!newPassword.equals(password)) {
            setPassword(newPassword);
        }
    }

    @Override
    public void onSuccessfulSignIn() {
        router.displaySnackbarMessage("");
        router.openStartPage();
    }

    @Override
    public void onSignInError() {
        router.displayErrorDialog("", "");
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    private void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    private void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

}
