package hu.rmatesz.movieaddict.presentation.auth.signup.di;

import dagger.Component;
import hu.rmatesz.movieaddict.common.di.ApplicationComponent;
import hu.rmatesz.movieaddict.common.di.PerActivity;
import hu.rmatesz.movieaddict.data.auth.di.AuthenticationDataComponent;
import hu.rmatesz.movieaddict.presentation.auth.signup.router.SignUpActivity;

/**
 * Created by Mate Redecsi on 2016. 10. 26..
 */
@PerActivity
@Component(dependencies = {ApplicationComponent.class, AuthenticationDataComponent.class}, modules = {SignUpModule.class})
public interface SignUpComponent {
    void inject(SignUpActivity activity);

    final class Injector {

        public static void inject(SignUpActivity activity) {
            ApplicationComponent applicationComponent = ApplicationComponent.Injector.getComponent();
            AuthenticationDataComponent authenticationDataComponent = applicationComponent.authenticationDataComponent().build();

            DaggerSignUpComponent.builder()
                    .applicationComponent(applicationComponent)
                    .authenticationDataComponent(authenticationDataComponent)
                    .signUpModule(new SignUpModule(activity))
                    .build()
                    .inject(activity);
        }
    }
}
