package hu.rmatesz.movieaddict.presentation.search.router;

import android.content.Intent;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import hu.rmatesz.movieaddict.R;
import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.common.IntentExtras;
import hu.rmatesz.movieaddict.databinding.SearchActivityBinding;
import hu.rmatesz.movieaddict.presentation.auth.signin.router.SignInActivity;
import hu.rmatesz.movieaddict.presentation.drawer.router.DrawerMenuRouter;
import hu.rmatesz.movieaddict.presentation.search.di.SearchComponent;
import hu.rmatesz.movieaddict.presentation.search.viewmodel.SearchViewModel;
import hu.rmatesz.movieaddict.presentation.series.details.router.SeriesDetailsActivity;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */

public class SearchActivity extends AppCompatActivity implements SearchRouter, DrawerMenuRouter {
    @Inject
    SearchViewModel searchViewModel;

    @Inject
    DataBindingComponent dataBindingComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SearchComponent.Injector.inject(this);

        DataBindingUtil.setDefaultComponent(dataBindingComponent);

        SearchActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.search_activity);
        binding.setViewModel(searchViewModel);
    }

    @Override
    protected void onStart() {
        super.onStart();
        searchViewModel.onStart();
    }

    @Override
    protected void onStop() {
        searchViewModel.onStop();
        super.onStop();
    }

    @Override
    public void openSeriesDetails(Series series, Bundle bundle) {
        Intent intent = new Intent(this, SeriesDetailsActivity.class);
        intent.putExtra(IntentExtras.SERIES, series);
        startActivity(intent, bundle);
    }

    @Override
    public void openSignIn() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }

}
