package hu.rmatesz.movieaddict.presentation.drawer.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.support.design.widget.NavigationView;
import android.view.View;

/**
 * Created by Mate Redecsi on 2016. 10. 27..
 */

public interface DrawerMenuViewModel extends Observable, NavigationView.OnNavigationItemSelectedListener {
    void signInClicked(View v);

    void onStart();

    void onStop();

    @Bindable
    boolean isAuthenticated();

    @Bindable
    String getUserEmail();
}
