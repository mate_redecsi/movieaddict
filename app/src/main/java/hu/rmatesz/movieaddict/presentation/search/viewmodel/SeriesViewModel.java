package hu.rmatesz.movieaddict.presentation.search.viewmodel;

import android.databinding.Bindable;

import java.util.List;

import hu.rmatesz.movieaddict.common.widget.recyclerview.ListItemViewModel;
import hu.rmatesz.movieaddict.common.widget.viewpager.ViewPagerItemViewModel;
import hu.rmatesz.movieaddict.presentation.search.model.SearchModel;

/**
 * Created by Mate_Redecsi on 12/25/2016.
 */

public interface SeriesViewModel extends ViewPagerItemViewModel, SearchModel.Callback {
    void onStart();
    void onStop();
    void onLastElementReached();

    @Bindable
    List<ListItemViewModel> getSearchResult();

    @Bindable
    boolean isLoading();
}
