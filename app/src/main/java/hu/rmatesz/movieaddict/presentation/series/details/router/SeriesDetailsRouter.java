package hu.rmatesz.movieaddict.presentation.series.details.router;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */

public interface SeriesDetailsRouter {
    void goBack();
}
