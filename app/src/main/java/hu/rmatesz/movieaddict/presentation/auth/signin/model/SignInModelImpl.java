package hu.rmatesz.movieaddict.presentation.auth.signin.model;

import java.util.ArrayList;
import java.util.List;

import hu.rmatesz.movieaddict.data.auth.model.User;
import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Mate Redecsi on 2016. 10. 26..
 */

public class SignInModelImpl implements SignInModel {
    private final AuthenticationDataProvider authenticationDataProvider;
    final List<Callback> callbacks = new ArrayList<>();
    Subscription signInSubscription;

    User pendingSignInResult;
    Throwable pendingSignInError;

    public SignInModelImpl(AuthenticationDataProvider authenticationDataProvider) {
        this.authenticationDataProvider = authenticationDataProvider;
    }

    @Override
    public void signIn(String email, String password) {
        if (signInSubscription != null && !signInSubscription.isUnsubscribed())
            return;

        signInSubscription = authenticationDataProvider.signIn(email, password)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleSignInResult, this::handleSignInError);
    }

    @Override
    public void stop() {
        if (signInSubscription != null && !signInSubscription.isUnsubscribed()) {
            signInSubscription.unsubscribe();
        }
    }

    @Override
    public void registerCallback(Callback callback) {
        callbacks.add(callback);

        if (pendingSignInResult != null) {
            handleSignInResult(pendingSignInResult);
        }
        if (pendingSignInError != null) {
            handleSignInError(pendingSignInError);
        }
    }

    @Override
    public void unregisterCallback(Callback callback) {
        callbacks.remove(callback);
    }

    private void handleSignInResult(User user) {
        if (callbacks.size() > 0) {
            for (Callback callback : callbacks) {
                callback.onSuccessfulSignIn();
            }
            pendingSignInResult = null;
        } else {
            pendingSignInResult = user;
        }
    }

    private void handleSignInError(Throwable e) {
        if (callbacks.size() > 0) {
            for (Callback callback : callbacks) {
                callback.onSignInError();
            }
            pendingSignInError = null;
        } else {
            pendingSignInError = e;
        }
    }
}
