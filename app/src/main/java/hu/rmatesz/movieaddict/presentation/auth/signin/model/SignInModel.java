package hu.rmatesz.movieaddict.presentation.auth.signin.model;

/**
 * Created by Mate Redecsi on 2016. 11. 03..
 */

public interface SignInModel {
    void signIn(String email, String password);
    void stop();
    void registerCallback(Callback callback);
    void unregisterCallback(Callback callback);

    interface Callback {
        void onSuccessfulSignIn();
        void onSignInError();
    }
}
