package hu.rmatesz.movieaddict.presentation.search.di;

import android.content.res.Resources;

import javax.inject.Provider;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.common.di.PerActivity;
import hu.rmatesz.movieaddict.data.movies.provider.MoviesDataProvider;
import hu.rmatesz.movieaddict.data.series.provider.SeriesDataProvider;
import hu.rmatesz.movieaddict.presentation.drawer.router.DrawerMenuRouter;
import hu.rmatesz.movieaddict.presentation.drawer.viewmodel.DrawerMenuViewModel;
import hu.rmatesz.movieaddict.presentation.search.model.SearchModel;
import hu.rmatesz.movieaddict.presentation.search.model.SearchModelImpl;
import hu.rmatesz.movieaddict.presentation.search.router.SearchActivity;
import hu.rmatesz.movieaddict.presentation.search.router.SearchRouter;
import hu.rmatesz.movieaddict.presentation.search.viewmodel.MovieItemViewModel;
import hu.rmatesz.movieaddict.presentation.search.viewmodel.MoviesViewModel;
import hu.rmatesz.movieaddict.presentation.search.viewmodel.MoviesViewModelImpl;
import hu.rmatesz.movieaddict.presentation.search.viewmodel.SeriesItemViewModel;
import hu.rmatesz.movieaddict.presentation.search.viewmodel.SearchViewModel;
import hu.rmatesz.movieaddict.presentation.search.viewmodel.SearchViewModelImpl;
import hu.rmatesz.movieaddict.presentation.search.viewmodel.SeriesViewModel;
import hu.rmatesz.movieaddict.presentation.search.viewmodel.SeriesViewModelImpl;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */
@Module
public class SearchModule {
    private final SearchActivity activity;

    public SearchModule(SearchActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    SearchRouter provideSearchRouter() {
        return activity;
    }

    @Provides
    @PerActivity
    DrawerMenuRouter provideDrawerMenuRouter() {
        return activity;
    }

    @Provides
    @PerActivity
    SearchViewModel provideSearchViewModel(SearchRouter router, SearchModel model, DrawerMenuViewModel drawerMenuViewModel,
            SeriesViewModel seriesViewModel, MoviesViewModel moviesViewModel) {
        return new SearchViewModelImpl(router, model, drawerMenuViewModel, seriesViewModel, moviesViewModel);
    }

    @Provides
    @PerActivity
    SearchModel provideSearchModel(SeriesDataProvider seriesDataProvider, MoviesDataProvider moviesDataProvider) {
        SearchModel searchModel = (SearchModel) activity.getLastCustomNonConfigurationInstance();
        return searchModel == null ? new SearchModelImpl(seriesDataProvider, moviesDataProvider) : searchModel;
    }

    @Provides
    @PerActivity
    SeriesViewModel provideSeriesViewModel(SearchModel searchModel, Provider<SeriesItemViewModel> itemViewModelProvider, Resources resources) {
        return new SeriesViewModelImpl(searchModel, itemViewModelProvider, resources);
    }

    @Provides
    SeriesItemViewModel provideSeriesItemViewModel(SearchRouter router) {
        return new SeriesItemViewModel(router);
    }

    @Provides
    @PerActivity
    MoviesViewModel provideMoviesViewModel(SearchModel searchModel, Provider<MovieItemViewModel> itemViewModelProvider, Resources resources) {
        return new MoviesViewModelImpl(searchModel, itemViewModelProvider, resources);
    }

    @Provides
    MovieItemViewModel provideMovieItemViewModel(SearchRouter router) {
        return new MovieItemViewModel(router);
    }

}
