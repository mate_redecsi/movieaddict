package hu.rmatesz.movieaddict.presentation.drawer.di;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.common.di.PerActivity;
import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;
import hu.rmatesz.movieaddict.presentation.drawer.model.DrawerMenuModel;
import hu.rmatesz.movieaddict.presentation.drawer.model.DrawerMenuModelImpl;
import hu.rmatesz.movieaddict.presentation.drawer.router.DrawerMenuRouter;
import hu.rmatesz.movieaddict.presentation.drawer.viewmodel.DrawerMenuViewModel;
import hu.rmatesz.movieaddict.presentation.drawer.viewmodel.DrawerMenuViewModelImpl;

/**
 * Created by Mate Redecsi on 2016. 10. 27..
 */
@Module
public class DrawerMenuModule {
    @Provides
    @PerActivity
    DrawerMenuViewModel drawerMenuViewModel(DrawerMenuRouter drawerMenuRouter, DrawerMenuModel drawerMenuModel) {
        return new DrawerMenuViewModelImpl(drawerMenuRouter, drawerMenuModel);
    }

    @Provides
    @PerActivity
    DrawerMenuModel drawerMenuModel(AuthenticationDataProvider authenticationDataProvider) {
        return new DrawerMenuModelImpl(authenticationDataProvider);
    }
}
