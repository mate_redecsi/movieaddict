package hu.rmatesz.movieaddict.presentation.search.viewmodel;

import android.content.res.Resources;

import java.util.ArrayList;

import javax.inject.Provider;

import hu.rmatesz.movieaddict.R;
import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.common.widget.recyclerview.ListItemViewModel;
import hu.rmatesz.movieaddict.presentation.search.model.SearchModel;

import static br.com.zbra.androidlinq.Linq.stream;

/**
 * Created by Mate_Redecsi on 12/26/2016.
 */

public class SeriesViewModelImpl extends SearchResultListViewModel implements SeriesViewModel {

    private final Provider<SeriesItemViewModel> itemViewModelProvider;
    private final Resources resources;

    public SeriesViewModelImpl(SearchModel model, Provider<SeriesItemViewModel> itemViewModelProvider, Resources resources) {
        super(model);
        this.itemViewModelProvider = itemViewModelProvider;
        this.resources = resources;
    }

    @Override
    public void onSearchStarted() {
        super.onSearchStarted();
        setSearchResult(new ArrayList<>());
    }

    @Override
    public void onSearchResultArrived(Series[] series) {
        super.onSearchResultArrived(series);
        setSearchResult(stream(series).select(item -> {
            SeriesItemViewModel seriesItemViewModel = itemViewModelProvider.get();
            seriesItemViewModel.setSeries(item);
            return (ListItemViewModel) seriesItemViewModel;
        }).toList());
    }

    @Override
    public void onSearchSeriesFinished() {
        setFinished(true);
        setLoading(false);
    }

    @Override
    public void onLastElementReached() {
        if (!isFinished() && !isLoading()) {
            model.loadNextSeriesPage();
            setLoading(true);
        }
    }

    @Override
    public String getTitle() {
        return resources.getString(R.string.series);
    }
}
