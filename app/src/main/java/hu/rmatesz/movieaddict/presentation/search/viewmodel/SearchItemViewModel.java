package hu.rmatesz.movieaddict.presentation.search.viewmodel;

import android.databinding.Bindable;
import android.view.View;

import hu.rmatesz.movieaddict.common.widget.recyclerview.SimpleListItemViewModel;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */

public abstract class SearchItemViewModel extends SimpleListItemViewModel {
    @Bindable
    public abstract String getPosterUrl();

    public abstract void onItemClicked(View v);

    @Bindable
    public abstract String getTitle();
}
