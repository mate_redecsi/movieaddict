package hu.rmatesz.movieaddict.presentation.search.viewmodel;

import android.databinding.BaseObservable;
import android.text.Editable;

import java.util.Arrays;
import java.util.List;

import hu.rmatesz.movieaddict.api.model.Movie;
import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.common.widget.viewpager.ViewPagerItemViewModel;
import hu.rmatesz.movieaddict.presentation.drawer.viewmodel.DrawerMenuViewModel;
import hu.rmatesz.movieaddict.presentation.search.model.SearchModel;
import hu.rmatesz.movieaddict.presentation.search.router.SearchRouter;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */

public class SearchViewModelImpl extends BaseObservable implements SearchViewModel {
    private final SearchRouter router;
    private final SearchModel model;
    private final DrawerMenuViewModel drawerMenuViewModel;
    private final SeriesViewModel seriesViewModel;
    private final MoviesViewModel moviesViewModel;

    public SearchViewModelImpl(SearchRouter router, SearchModel model, DrawerMenuViewModel drawerMenuViewModel, SeriesViewModel seriesViewModel, MoviesViewModel moviesViewModel) {
        this.router = router;
        this.model = model;
        this.drawerMenuViewModel = drawerMenuViewModel;
        this.seriesViewModel = seriesViewModel;
        this.moviesViewModel = moviesViewModel;
    }

    @Override
    public void onSearchTextChanged(Editable s) {
        if (s.length() > 1) {
            model.search(s.toString());
        }
    }

    @Override
    public void onStart() {
        model.registerCallback(this);
        drawerMenuViewModel.onStart();
        seriesViewModel.onStart();
        moviesViewModel.onStart();
    }

    @Override
    public void onStop() {
        model.unregisterCallback(this);
        drawerMenuViewModel.onStop();
    }

    @Override
    public DrawerMenuViewModel getDrawerMenuViewModel() {
        return drawerMenuViewModel;
    }

    @Override
    public List<ViewPagerItemViewModel> getContentViewModels() {
        return Arrays.asList(seriesViewModel, moviesViewModel);
    }

    @Override
    public void onSearchStarted() {
        // no-op
    }

    @Override
    public void onSearchResultArrived(Series[] series) {
        // no-op
    }

    @Override
    public void onSearchResultArrived(Movie[] movies) {
        // no-op
    }

    @Override
    public void onSearchSeriesFinished() {
        // no-op
    }

    @Override
    public void onSearchMoviesFinished() {
        // no-op
    }

    @Override
    public void onSearchError() {
        // no-op
    }

}
