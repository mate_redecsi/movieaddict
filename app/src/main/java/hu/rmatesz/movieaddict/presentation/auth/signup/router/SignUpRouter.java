package hu.rmatesz.movieaddict.presentation.auth.signup.router;

/**
 * Created by Mate Redecsi on 2016. 11. 03..
 */
public interface SignUpRouter {
    void openStartPage();
    void displayErrorDialog(String title, String message);
    void displaySnackbarMessage(String message);
}
