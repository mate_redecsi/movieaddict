package hu.rmatesz.movieaddict.presentation.search.viewmodel;

import android.content.res.Resources;

import java.util.ArrayList;

import javax.inject.Provider;

import hu.rmatesz.movieaddict.R;
import hu.rmatesz.movieaddict.api.model.Movie;
import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.common.widget.recyclerview.ListItemViewModel;
import hu.rmatesz.movieaddict.presentation.search.model.SearchModel;

import static br.com.zbra.androidlinq.Linq.stream;

/**
 * Created by Mate_Redecsi on 12/26/2016.
 */

public class MoviesViewModelImpl extends SearchResultListViewModel implements MoviesViewModel {
    private final Provider<MovieItemViewModel> itemViewModelProvider;
    private final Resources resources;

    public MoviesViewModelImpl(SearchModel model, Provider<MovieItemViewModel> itemViewModelProvider, Resources resources) {
        super(model);
        this.itemViewModelProvider = itemViewModelProvider;
        this.resources = resources;
    }

    @Override
    public void onSearchStarted() {
        super.onSearchStarted();
        setSearchResult(new ArrayList<>());
    }

    @Override
    public void onSearchResultArrived(Series[] series) {
        // no-op
    }

    @Override
    public void onSearchResultArrived(Movie[] movies) {
        setSearchResult(stream(movies).select(item -> {
            MovieItemViewModel movieItemViewModel = itemViewModelProvider.get();
            movieItemViewModel.setMovie(item);
            return (ListItemViewModel) movieItemViewModel;
        }).toList());
    }

    @Override
    public void onSearchSeriesFinished() {
        // no-op
    }

    @Override
    public void onSearchMoviesFinished() {
        setFinished(true);
        setLoading(false);
    }

    @Override
    public void onSearchSeriesError() {

    }

    @Override
    public void onSearchMoviesError() {

    }

    @Override
    public void onLastElementReached() {
        if (!isFinished() && !isLoading()) {
            model.loadNextMoviesPage();
            setLoading(true);
        }
    }

    @Override
    public String getTitle() {
        return resources.getString(R.string.movies);
    }
}
