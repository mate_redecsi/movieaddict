package hu.rmatesz.movieaddict.presentation.search.model;

import java.util.ArrayList;
import java.util.List;

import hu.rmatesz.movieaddict.api.model.Movie;
import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.data.movies.provider.MoviesDataProvider;
import hu.rmatesz.movieaddict.data.series.provider.SeriesDataProvider;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */

public class SearchModelImpl implements SearchModel {
    private final List<Callback> callbacks = new ArrayList<>();
    private final SeriesDataProvider seriesDataProvider;
    private final MoviesDataProvider moviesDataProvider;
    private Subscription searchSeriesSubscription;
    private Subscription searchMoviesSubscription;
    private List<Series> series;
    private List<Movie> movies;
    private boolean pendingSeriesResult;
    private boolean pendingMoviesResult;
    private Throwable pendingError;
    private boolean pendingSearchStart;
    private boolean pendingSearchSeriesFinish;
    private boolean pendingSearchMoviesFinish;

    public SearchModelImpl(SeriesDataProvider seriesDataProvider, MoviesDataProvider moviesDataProvider) {
        this.seriesDataProvider = seriesDataProvider;
        this.moviesDataProvider = moviesDataProvider;
    }

    @Override
    public void search(String title) {
        stop();

        searchSeries(title);
        searchMovies(title);

        notifySearchStarted();
    }

    @Override
    public void loadNextSeriesPage() {
        if (searchSeriesSubscription == null || searchSeriesSubscription.isUnsubscribed()) {
            return;
        }

        seriesDataProvider.loadPage();
    }

    @Override
    public void loadNextMoviesPage() {
        if (searchMoviesSubscription == null || searchMoviesSubscription.isUnsubscribed()) {
            return;
        }

        moviesDataProvider.loadPage();
    }

    @Override
    public void stop() {
        if (searchSeriesSubscription != null && !searchSeriesSubscription.isUnsubscribed()) {
            searchSeriesSubscription.unsubscribe();
        }

        if (searchMoviesSubscription != null && !searchMoviesSubscription.isUnsubscribed()) {
            searchMoviesSubscription.unsubscribe();
        }
    }

    @Override
    public void registerCallback(Callback callback) {
        callbacks.add(callback);
        if (pendingSeriesResult) {
            notifySearchSeriesResult();
        }
        if (pendingMoviesResult) {
            notifySearchMoviesResult();
        }
        if (pendingError != null) {
            notifySearchError(pendingError);
        }
        if (pendingSearchSeriesFinish) {
            notifySearchSeriesFinished();
        }
        if (pendingSearchMoviesFinish) {
            notifySearchMoviesFinished();
        }
        if (pendingSearchStart) {
            notifySearchStarted();
        }
    }

    @Override
    public void unregisterCallback(Callback callback) {
        callbacks.remove(callback);
    }

    private void searchSeries(String title) {
        series = new ArrayList<>();

        searchSeriesSubscription = seriesDataProvider.searchSeries(title)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleSearchSeriesResult, this::notifySearchError, this::notifySearchSeriesFinished);

        seriesDataProvider.loadPage();
    }

    private void searchMovies(String title) {
        movies = new ArrayList<>();

        searchMoviesSubscription = moviesDataProvider.searchMovies(title)
                .subscribeOn(Schedulers.computation())
                .unsubscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleSearchMoviesResult, this::notifySearchError, this::notifySearchMoviesFinished);
    }

    private void handleSearchSeriesResult(List<Series> series) {
        this.series.addAll(series);
        notifySearchSeriesResult();
    }

    private void handleSearchMoviesResult(List<Movie> movies) {
        this.movies.addAll(movies);
        notifySearchMoviesResult();
    }

    private void notifySearchStarted() {
        if (callbacks.size() == 0) {
            pendingSearchStart = true;
        } else {
            for (Callback callback : callbacks) {
                callback.onSearchStarted();
            }
            pendingSearchStart = false;
        }
    }

    private void notifySearchError(Throwable e) {
        if (callbacks.size() == 0) {
            pendingError = e;
        } else {
            for (Callback callback : callbacks) {
                callback.onSearchError();
            }
            pendingError = null;
        }
    }

    private void notifySearchSeriesResult() {
        if (callbacks.size() == 0) {
            pendingSeriesResult = true;
        } else {
            for (Callback callback : callbacks) {
                callback.onSearchResultArrived(series.toArray(new Series[series.size()]));
            }
            pendingSeriesResult = false;
        }
    }

    private void notifySearchMoviesResult() {
        if (callbacks.size() == 0) {
            pendingMoviesResult = true;
        } else {
            for (Callback callback : callbacks) {
                callback.onSearchResultArrived(movies.toArray(new Movie[movies.size()]));
            }
            pendingMoviesResult = false;
        }
    }

    private void notifySearchSeriesFinished() {
        if (callbacks.size() == 0) {
            pendingSearchSeriesFinish = true;
        } else {
            for (Callback callback : callbacks) {
                callback.onSearchSeriesFinished();
            }
            pendingSearchSeriesFinish = false;
        }
    }

    private void notifySearchMoviesFinished() {
        if (callbacks.size() == 0) {
            pendingSearchMoviesFinish = true;
        } else {
            for (Callback callback : callbacks) {
                callback.onSearchMoviesFinished();
            }
            pendingSearchMoviesFinish = false;
        }
    }
}
