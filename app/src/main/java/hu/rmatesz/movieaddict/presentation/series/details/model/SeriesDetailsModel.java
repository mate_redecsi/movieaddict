package hu.rmatesz.movieaddict.presentation.series.details.model;

import hu.rmatesz.movieaddict.api.model.Series;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */

public interface SeriesDetailsModel {
    void loadSeries();
    Series getSeries();
    void stop();

    boolean isFavourite();
    void setFavourite(boolean favourite);

    void registerCallback(Callback callback);
    void unregisterCallback(Callback callback);

    interface Callback {
        void onSeriesLoaded(Series series);
    }
}
