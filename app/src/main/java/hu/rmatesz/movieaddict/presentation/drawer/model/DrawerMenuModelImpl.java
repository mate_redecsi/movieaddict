package hu.rmatesz.movieaddict.presentation.drawer.model;

import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;

/**
 * Created by Mate_Redecsi on 11/9/2016.
 */

public class DrawerMenuModelImpl implements DrawerMenuModel {
    private final AuthenticationDataProvider authenticationDataProvider;

    public DrawerMenuModelImpl(AuthenticationDataProvider authenticationDataProvider) {
        this.authenticationDataProvider = authenticationDataProvider;
    }

    @Override
    public void signOut() {
        authenticationDataProvider.signOut();
    }
}
