package hu.rmatesz.movieaddict.presentation.series.details.di;

import android.content.Intent;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.common.IntentExtras;
import hu.rmatesz.movieaddict.common.di.PerActivity;
import hu.rmatesz.movieaddict.data.series.provider.SeriesDataProvider;
import hu.rmatesz.movieaddict.presentation.series.details.model.SeriesDetailsModel;
import hu.rmatesz.movieaddict.presentation.series.details.model.SeriesDetailsModelImpl;
import hu.rmatesz.movieaddict.presentation.series.details.router.SeriesDetailsActivity;
import hu.rmatesz.movieaddict.presentation.series.details.router.SeriesDetailsRouter;
import hu.rmatesz.movieaddict.presentation.series.details.viewmodel.SeriesDetailsViewModel;
import hu.rmatesz.movieaddict.presentation.series.details.viewmodel.SeriesDetailsViewModelImpl;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */
@Module
public class SeriesDetailsModule {
    private final SeriesDetailsActivity activity;

    public SeriesDetailsModule(SeriesDetailsActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    public SeriesDetailsRouter provideSeriesDetailsRouter() {
        return activity;
    }

    @Provides
    @PerActivity
    public SeriesDetailsViewModel provideSeriesDetailsViewModel(SeriesDetailsRouter seriesDetailsRouter, SeriesDetailsModel seriesDetailsModel) {
        return new SeriesDetailsViewModelImpl(seriesDetailsRouter, seriesDetailsModel);
    }

    @Provides
    @PerActivity
    public SeriesDetailsModel provideSeriesDetailsModel(SeriesDataProvider seriesDataProvider, @Named("seriesExtra") Series series) {
        return new SeriesDetailsModelImpl(seriesDataProvider, series);
    }

    @Provides
    @Named("seriesExtra")
    public Series provideSeriesExtra(Intent intent) {
        return (Series)intent.getSerializableExtra(IntentExtras.SERIES);
    }

    @Provides
    public Intent provideIntent() {
        return activity.getIntent();
    }
}
