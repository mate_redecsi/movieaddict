package hu.rmatesz.movieaddict.presentation.series.details.model;

import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.data.series.provider.SeriesDataProvider;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */

public class SeriesDetailsModelImpl implements SeriesDetailsModel {
    private final SeriesDataProvider seriesDataProvider;
    private Series series;
    private Subscription loadSubscription;

    public SeriesDetailsModelImpl(SeriesDataProvider seriesDataProvider, Series series) {
        this.seriesDataProvider = seriesDataProvider;
        this.series = series;
    }

    @Override
    public void loadSeries() {
        if (loadSubscription != null && !loadSubscription.isUnsubscribed()) {
            return;
        }

        loadSubscription = seriesDataProvider.getSeriesById(series.getImdbId())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(series -> {
                    this.series = series;
                }, e -> { });
    }

    @Override
    public Series getSeries() {
        return series;
    }

    @Override
    public void stop() {

    }

    @Override
    public boolean isFavourite() {
        return false;
    }

    @Override
    public void setFavourite(boolean favourite) {

    }

    @Override
    public void registerCallback(Callback callback) {

    }

    @Override
    public void unregisterCallback(Callback callback) {

    }
}
