package hu.rmatesz.movieaddict.presentation.auth.signin.router;

/**
 * Created by Mate Redecsi on 2016. 10. 26..
 */

public interface SignInRouter {
    void openSignUp();

    void displaySnackbarMessage(String message);

    void openStartPage();

    void displayErrorDialog(String title, String message);
}
