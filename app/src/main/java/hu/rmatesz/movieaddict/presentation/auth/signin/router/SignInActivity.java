package hu.rmatesz.movieaddict.presentation.auth.signin.router;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import hu.rmatesz.movieaddict.BR;
import hu.rmatesz.movieaddict.R;
import hu.rmatesz.movieaddict.databinding.SignInActivityBinding;
import hu.rmatesz.movieaddict.presentation.auth.signin.di.SignInComponent;
import hu.rmatesz.movieaddict.presentation.auth.signin.viewmodel.SignInViewModel;
import hu.rmatesz.movieaddict.presentation.auth.signup.router.SignUpActivity;

/**
 * Created by Mate Redecsi on 2016. 10. 26..
 */

public class SignInActivity extends AppCompatActivity implements SignInRouter {
    private final static int SIGN_UP_REQUEST = 1234;

    @Inject
    SignInViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SignInComponent.Injector.inject(this);

        SignInActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.sign_in_activity);
        binding.setVariable(BR.viewModel, viewModel);
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.onStart();
    }

    @Override
    protected void onStop() {
        viewModel.onStop();
        super.onStop();
    }

    @Override
    public void openSignUp() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivityForResult(intent, SIGN_UP_REQUEST);
    }

    @Override
    public void displaySnackbarMessage(String message) {

    }

    @Override
    public void openStartPage() {

    }

    @Override
    public void displayErrorDialog(String title, String message) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SIGN_UP_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
}
