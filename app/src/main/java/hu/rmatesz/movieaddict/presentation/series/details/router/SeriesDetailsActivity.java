package hu.rmatesz.movieaddict.presentation.series.details.router;

import android.annotation.TargetApi;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.view.DraweeTransition;

import javax.inject.Inject;

import hu.rmatesz.movieaddict.R;
import hu.rmatesz.movieaddict.databinding.SeriesDetailsActivityBinding;
import hu.rmatesz.movieaddict.presentation.series.details.di.SeriesDetailsComponent;
import hu.rmatesz.movieaddict.presentation.series.details.viewmodel.SeriesDetailsViewModel;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */

public class SeriesDetailsActivity extends AppCompatActivity implements SeriesDetailsRouter {
    @Inject
    SeriesDetailsViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SeriesDetailsComponent.Injector.inject(this);

        SeriesDetailsActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.series_details_activity);
        binding.setViewModel(viewModel);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupTransition();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return viewModel;
    }

    @Override
    public void goBack() {
        finish();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setupTransition() {
        getWindow().setSharedElementEnterTransition(DraweeTransition.createTransitionSet(ScalingUtils.ScaleType.CENTER_CROP,
                ScalingUtils.ScaleType.CENTER_CROP));
        getWindow().setSharedElementReturnTransition(DraweeTransition.createTransitionSet(ScalingUtils.ScaleType.CENTER_CROP,
                ScalingUtils.ScaleType.CENTER_CROP));
    }
}
