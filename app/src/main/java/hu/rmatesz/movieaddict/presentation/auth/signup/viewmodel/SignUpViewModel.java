package hu.rmatesz.movieaddict.presentation.auth.signup.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.text.Editable;
import android.view.View;

import hu.rmatesz.movieaddict.presentation.auth.signup.model.SignUpModel;

/**
 * Created by Mate Redecsi on 2016. 11. 03..
 */

public interface SignUpViewModel extends SignUpModel.Callback, Observable {
    void onStart();
    void onStop();
    void onSignUpClicked(View v);
    void onEmailChanged(Editable s);
    void onPasswordChanged(Editable s);
    void onConfirmPasswordChanged(Editable s);
    @Bindable
    String getEmail();
    @Bindable
    String getPassword();
    @Bindable
    String getConfirmPassword();
    @Bindable
    String getEmailError();
    @Bindable
    String getPasswordError();
    @Bindable
    String getConfirmPasswordError();
}
