package hu.rmatesz.movieaddict.presentation.search.viewmodel;

import android.view.View;

import hu.rmatesz.movieaddict.api.model.Movie;
import hu.rmatesz.movieaddict.presentation.search.router.SearchRouter;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */
public class MovieItemViewModel extends SearchItemViewModel {
    private final SearchRouter router;
    private Movie movie;

    public MovieItemViewModel(SearchRouter router) {
        this.router = router;
    }

    @Override
    public String getPosterUrl() {
        return movie.getPoster();
    }

    @Override
    public void onItemClicked(View v) {
//        ActivityOptionsCompat options = ActivityOptionsCompat.
//                makeSceneTransitionAnimation(router.getActivity(), v.findViewById(R.id.poster), "poster");
//        router.openSeriesDetails(series, null);
    }

    @Override
    public String getTitle() {
        return movie.getTitle();
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
