package hu.rmatesz.movieaddict.presentation.auth.signup.di;

import android.databinding.DataBindingUtil;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.BR;
import hu.rmatesz.movieaddict.R;
import hu.rmatesz.movieaddict.common.di.PerActivity;
import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;
import hu.rmatesz.movieaddict.databinding.SignUpActivityBinding;
import hu.rmatesz.movieaddict.presentation.auth.signup.model.SignUpModel;
import hu.rmatesz.movieaddict.presentation.auth.signup.model.SignUpModelImpl;
import hu.rmatesz.movieaddict.presentation.auth.signup.router.SignUpActivity;
import hu.rmatesz.movieaddict.presentation.auth.signup.router.SignUpRouter;
import hu.rmatesz.movieaddict.presentation.auth.signup.viewmodel.SignUpViewModel;
import hu.rmatesz.movieaddict.presentation.auth.signup.viewmodel.SignUpViewModelImpl;
import hu.rmatesz.movieaddict.util.EmailValidator;
import hu.rmatesz.movieaddict.util.PasswordValidator;

/**
 * Created by Mate Redecsi on 2016. 10. 26..
 */
@Module
public class SignUpModule {
    private final SignUpActivity activity;

    public SignUpModule(SignUpActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    SignUpRouter provideSignUpRouter() {
        return activity;
    }

    @Provides
    @PerActivity
    SignUpViewModel provideSignUpViewModel(SignUpRouter router, SignUpModel model) {
        return new SignUpViewModelImpl(router, model);
    }

    @Provides
    @PerActivity
    SignUpModel provideSignUpModel(AuthenticationDataProvider authenticationDataProvider, EmailValidator emailValidator, PasswordValidator passwordValidator) {
        return new SignUpModelImpl(authenticationDataProvider, emailValidator, passwordValidator);
    }

    @Provides
    @PerActivity
    SignUpActivityBinding provideSignUpActivityBinding(SignUpViewModel viewModel) {
        SignUpActivityBinding binding = DataBindingUtil.setContentView(activity, R.layout.sign_up_activity);
        binding.setVariable(BR.viewModel, viewModel);
        return binding;
    }
}
