package hu.rmatesz.movieaddict.presentation.series.details.di;

import dagger.Component;
import hu.rmatesz.movieaddict.common.di.ApplicationComponent;
import hu.rmatesz.movieaddict.common.di.PerActivity;
import hu.rmatesz.movieaddict.data.series.di.SeriesDataComponent;
import hu.rmatesz.movieaddict.presentation.series.details.router.SeriesDetailsActivity;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */
@Component(dependencies = {ApplicationComponent.class, SeriesDataComponent.class}, modules = {SeriesDetailsModule.class})
@PerActivity
public interface SeriesDetailsComponent {
    void inject(SeriesDetailsActivity activity);

    final class Injector {
        public static void inject(SeriesDetailsActivity activity) {
            ApplicationComponent applicationComponent = ApplicationComponent.Injector.getComponent();
            SeriesDataComponent seriesDataComponent = applicationComponent.seriesDataComponent().build();
            DaggerSeriesDetailsComponent.builder()
                    .applicationComponent(applicationComponent)
                    .seriesDataComponent(seriesDataComponent)
                    .seriesDetailsModule(new SeriesDetailsModule(activity))
                    .build()
                    .inject(activity);
        }
    }
}
