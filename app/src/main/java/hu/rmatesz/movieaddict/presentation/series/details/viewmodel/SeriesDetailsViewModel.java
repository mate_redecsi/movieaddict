package hu.rmatesz.movieaddict.presentation.series.details.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;

/**
 * Created by Mate_Redecsi on 10/23/2016.
 */

public interface SeriesDetailsViewModel extends Observable {
    void onRefresh();
    void onStart();
    void onStop();

    @Bindable
    String getTitle();

    @Bindable
    String getPosterUrl();

    @Bindable
    boolean isFavourite();
}
