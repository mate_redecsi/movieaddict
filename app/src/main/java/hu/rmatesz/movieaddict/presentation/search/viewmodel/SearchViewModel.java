package hu.rmatesz.movieaddict.presentation.search.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.text.Editable;

import java.util.List;

import hu.rmatesz.movieaddict.common.widget.viewpager.ViewPagerItemViewModel;
import hu.rmatesz.movieaddict.presentation.drawer.viewmodel.DrawerMenuViewModel;
import hu.rmatesz.movieaddict.presentation.search.model.SearchModel;

/**
 * Created by Mate_Redecsi on 11/4/2016.
 */

public interface SearchViewModel extends Observable, SearchModel.Callback {
    void onSearchTextChanged(Editable s);

    void onStart();

    void onStop();

    @Bindable
    DrawerMenuViewModel getDrawerMenuViewModel();

    @Bindable
    List<ViewPagerItemViewModel> getContentViewModels();

}
