package hu.rmatesz.movieaddict.presentation.auth.signin.di;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.common.di.PerActivity;
import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;
import hu.rmatesz.movieaddict.presentation.auth.signin.model.SignInModel;
import hu.rmatesz.movieaddict.presentation.auth.signin.model.SignInModelImpl;
import hu.rmatesz.movieaddict.presentation.auth.signin.router.SignInActivity;
import hu.rmatesz.movieaddict.presentation.auth.signin.router.SignInRouter;
import hu.rmatesz.movieaddict.presentation.auth.signin.viewmodel.SignInViewModel;
import hu.rmatesz.movieaddict.presentation.auth.signin.viewmodel.SignInViewModelImpl;

/**
 * Created by Mate Redecsi on 2016. 10. 26..
 */
@Module
public class SignInModule {
    private final SignInActivity activity;

    public SignInModule(SignInActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    SignInRouter provideSignInRouter() {
        return activity;
    }

    @Provides
    @PerActivity
    SignInViewModel provideSignInViewModel(SignInRouter router, SignInModel model) {
        return new SignInViewModelImpl(router, model);
    }

    @Provides
    @PerActivity
    SignInModel provideSignInModel(AuthenticationDataProvider authenticationDataProvider) {
        return new SignInModelImpl(authenticationDataProvider);
    }
}
