package hu.rmatesz.movieaddict.presentation.drawer.router;

/**
 * Created by Mate Redecsi on 2016. 10. 27..
 */

public interface DrawerMenuRouter {
    void openSignIn();
}
