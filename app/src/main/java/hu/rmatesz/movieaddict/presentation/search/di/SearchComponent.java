package hu.rmatesz.movieaddict.presentation.search.di;

import dagger.Component;
import hu.rmatesz.movieaddict.common.databinding.di.DatabindingModule;
import hu.rmatesz.movieaddict.common.di.ActivityModule;
import hu.rmatesz.movieaddict.common.di.ApplicationComponent;
import hu.rmatesz.movieaddict.common.di.PerActivity;
import hu.rmatesz.movieaddict.data.auth.di.AuthenticationDataComponent;
import hu.rmatesz.movieaddict.data.movies.di.MoviesDataComponent;
import hu.rmatesz.movieaddict.data.series.di.SeriesDataComponent;
import hu.rmatesz.movieaddict.presentation.drawer.di.DrawerMenuModule;
import hu.rmatesz.movieaddict.presentation.search.router.SearchActivity;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */
@PerActivity
@Component(dependencies = {SeriesDataComponent.class, MoviesDataComponent.class, AuthenticationDataComponent.class, ApplicationComponent.class}, modules = {
        ActivityModule.class, DatabindingModule.class, SearchModule.class, DrawerMenuModule.class})
public interface SearchComponent {
    void inject(SearchActivity searchActivity);

    final class Injector {
        public static void inject(SearchActivity searchActivity) {
            ApplicationComponent applicationComponent = ApplicationComponent.Injector.getComponent();
            AuthenticationDataComponent authenticationDataComponent = applicationComponent.authenticationDataComponent().build();
            SeriesDataComponent seriesDataComponent = applicationComponent.seriesDataComponent().build();
            MoviesDataComponent moviesDataComponent = applicationComponent.moviesDataComponent().build();

            DaggerSearchComponent.builder()
                    .authenticationDataComponent(authenticationDataComponent)
                    .seriesDataComponent(seriesDataComponent)
                    .moviesDataComponent(moviesDataComponent)
                    .applicationComponent(applicationComponent)
                    .activityModule(new ActivityModule(searchActivity))
                    .searchModule(new SearchModule(searchActivity))
                    .build()
                    .inject(searchActivity);
        }
    }
}
