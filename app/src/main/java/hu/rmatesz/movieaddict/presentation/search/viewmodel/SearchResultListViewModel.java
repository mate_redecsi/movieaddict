package hu.rmatesz.movieaddict.presentation.search.viewmodel;

import android.databinding.BaseObservable;

import java.util.List;

import hu.rmatesz.movieaddict.BR;
import hu.rmatesz.movieaddict.common.widget.recyclerview.ListItemViewModel;
import hu.rmatesz.movieaddict.presentation.search.model.SearchModel;

/**
 * Created by Mate_Redecsi on 12/26/2016.
 */

public abstract class SearchResultListViewModel extends BaseObservable implements SearchModel.Callback {
    final SearchModel model;
    private boolean finished;
    private boolean loading;
    private List<ListItemViewModel> searchResult;

    public SearchResultListViewModel(SearchModel model) {
        this.model = model;
    }

    public void onStart() {
        model.registerCallback(this);
    }

    public void onStop() {
        model.unregisterCallback(this);
    }

    @Override
    public void onSearchStarted() {
        finished = false;
    }

    public List<ListItemViewModel> getSearchResult() {
        return searchResult;
    }

    public boolean isLoading() {
        return loading;
    }

    protected boolean isFinished() {
        return finished;
    }

    void setSearchResult(List<ListItemViewModel> searchResult) {
        this.searchResult = searchResult;
        notifyPropertyChanged(BR.searchResult);
    }

    void setLoading(boolean loading) {
        this.loading = loading;
        notifyPropertyChanged(BR.loading);
    }

    void setFinished(boolean finished) {
        this.finished = finished;
    }
}
