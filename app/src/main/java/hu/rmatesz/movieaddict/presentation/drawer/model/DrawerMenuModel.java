package hu.rmatesz.movieaddict.presentation.drawer.model;

/**
 * Created by Mate_Redecsi on 11/9/2016.
 */

public interface DrawerMenuModel {
    void signOut();
}
