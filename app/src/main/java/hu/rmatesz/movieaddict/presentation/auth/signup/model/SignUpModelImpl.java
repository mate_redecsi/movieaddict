package hu.rmatesz.movieaddict.presentation.auth.signup.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import hu.rmatesz.movieaddict.data.auth.model.User;
import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;
import hu.rmatesz.movieaddict.util.EmailValidator;
import hu.rmatesz.movieaddict.util.PasswordValidator;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Mate_Redecsi on 11/12/2016.
 */

public class SignUpModelImpl implements SignUpModel {
    final List<Callback> callbacks = new ArrayList<>();
    private final AuthenticationDataProvider authenticationDataProvider;

    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;

    EmailError pendingEmailError;
    PasswordError pendingPasswordError;
    boolean pendingPasswordMismatchError;
    User pendingSuccessfulSignUp;
    Throwable pendingSignUpError;

    Subscription signUpSubscription;

    public SignUpModelImpl(AuthenticationDataProvider authenticationDataProvider, EmailValidator emailValidator, PasswordValidator passwordValidator) {
        this.authenticationDataProvider = authenticationDataProvider;
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
    }

    @Override
    public void validateEmail(String email) {
        EmailValidator.ValidationResult result = emailValidator.validateEmail(email);
        switch (result) {
            case EMPTY:
                notifyEmailError(EmailError.EMPTY);
                break;
            case INVALID_FORMAT:
                notifyEmailError(EmailError.INVALID_FORMAT);
                break;
        }
    }

    @Override
    public void validatePassword(String password) {
        PasswordValidator.ValidationResult result = passwordValidator.validatePassword(password);
        switch (result) {
            case EMPTY:
                notifyPasswordError(PasswordError.EMPTY);
                break;
            case TOO_SHORT:
                notifyPasswordError(PasswordError.TOO_SHORT);
                break;
        }
    }

    @Override
    public void validateConfirmPassword(String password, @NonNull String confirmPassword) {
        if (!confirmPassword.equals(password))
            notifyPasswordMismatchError();
    }

    @Override
    public void signUp(@NonNull String email, @NonNull String password) {
        if (signUpSubscription != null && !signUpSubscription.isUnsubscribed())
            return;

        signUpSubscription = authenticationDataProvider.signUp(email, password)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::notifySignUpResult, this::notifySignUpError);

    }

    @Override
    public void stop() {
        if (signUpSubscription == null || signUpSubscription.isUnsubscribed())
            return;

        signUpSubscription.unsubscribe();
    }

    @Override
    public void registerCallback(Callback callback) {
        callbacks.add(callback);
        if (pendingEmailError != null) {
            notifyEmailError(pendingEmailError);
        }
        if (pendingPasswordError != null) {
            notifyPasswordError(pendingPasswordError);
        }
        if (pendingPasswordMismatchError) {
            notifyPasswordMismatchError();
        }
        if (pendingSuccessfulSignUp != null) {
            notifySignUpResult(pendingSuccessfulSignUp);
        }
        if (pendingSignUpError != null) {
            notifySignUpError(pendingSignUpError);
        }
    }

    @Override
    public void unregisterCallback(Callback callback) {
        callbacks.remove(callback);
    }

    private void notifySignUpResult(User user) {
        if (callbacks.size() > 0) {
            for (Callback callback : callbacks) {
                callback.onSuccessfulSignUp();
            }
            this.pendingSuccessfulSignUp = null;
        } else {
            this.pendingSuccessfulSignUp = user;
        }
    }

    private void notifySignUpError(Throwable throwable) {
        if (callbacks.size() > 0) {
            for (Callback callback : callbacks) {
                callback.onSignUpError();
            }
            this.pendingSignUpError = null;
        } else {
            this.pendingSignUpError = throwable;
        }
    }

    private void notifyEmailError(EmailError emailError) {
        if (callbacks.size() > 0) {
            for (Callback callback : callbacks) {
                callback.onEmailValidationError(emailError);
            }
            this.pendingEmailError = null;
        } else {
            this.pendingEmailError = emailError;
        }
    }

    private void notifyPasswordError(PasswordError passwordError) {
        if (callbacks.size() > 0) {
            for (Callback callback : callbacks) {
                callback.onPasswordValidationError(passwordError);
            }
            this.pendingPasswordError = null;
        } else {
            this.pendingPasswordError = passwordError;
        }
    }

    private void notifyPasswordMismatchError() {
        if (callbacks.size() > 0) {
            for (Callback callback : callbacks) {
                callback.onConfirmPasswordValidationError();
            }
            this.pendingPasswordMismatchError = false;
        } else {
            this.pendingPasswordMismatchError = true;
        }
    }
}
