package hu.rmatesz.movieaddict.presentation.search.model;

import hu.rmatesz.movieaddict.api.model.Movie;
import hu.rmatesz.movieaddict.api.model.Series;

/**
 * Created by Mate_Redecsi on 11/4/2016.
 */

public interface SearchModel {
    void search(String title);
    void loadNextSeriesPage();
    void loadNextMoviesPage();
    void stop();
    void registerCallback(Callback callback);
    void unregisterCallback(Callback callback);

    interface Callback {
        void onSearchStarted();
        void onSearchResultArrived(Series[] series);
        void onSearchResultArrived(Movie[] movies);
        void onSearchSeriesFinished();
        void onSearchMoviesFinished();
        void onSearchSeriesError();
        void onSearchMoviesError();
    }
}
