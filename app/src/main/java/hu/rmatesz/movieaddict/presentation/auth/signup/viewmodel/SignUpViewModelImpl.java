package hu.rmatesz.movieaddict.presentation.auth.signup.viewmodel;

import android.databinding.BaseObservable;
import android.text.Editable;
import android.view.View;

import hu.rmatesz.movieaddict.BR;
import hu.rmatesz.movieaddict.presentation.auth.signup.model.SignUpModel;
import hu.rmatesz.movieaddict.presentation.auth.signup.router.SignUpRouter;

/**
 * Created by Mate_Redecsi on 11/12/2016.
 */

public class SignUpViewModelImpl extends BaseObservable implements SignUpViewModel {
    private final SignUpRouter router;
    private final SignUpModel model;

    String email;
    String password;

    private String confirmPassword;
    private String emailError;
    private String passwordError;
    private String confirmPasswordError;

    public SignUpViewModelImpl(SignUpRouter router, SignUpModel model) {
        this.router = router;
        this.model = model;
    }

    @Override
    public void onStart() {
        model.registerCallback(this);
    }

    @Override
    public void onStop() {
        model.unregisterCallback(this);
    }

    @Override
    public void onSignUpClicked(View v) {
        model.signUp(email, password);
    }

    @Override
    public void onEmailChanged(Editable s) {
        setEmail(s.toString());
        model.validateEmail(email);
    }

    @Override
    public void onPasswordChanged(Editable s) {
        setPassword(s.toString());
        model.validatePassword(password);
    }

    @Override
    public void onConfirmPasswordChanged(Editable s) {
        setConfirmPassword(s.toString());
        model.validateConfirmPassword(password, confirmPassword);
    }

    @Override
    public void onSuccessfulSignUp() {
        router.displaySnackbarMessage("successfully signed up");
        router.openStartPage();
    }

    @Override
    public void onSignUpError() {
        // TODO: display proper error message
        router.displayErrorDialog("Sign up error", "Unable to sign up!");
    }

    @Override
    public void onEmailValidationError(SignUpModel.EmailError emailError) {
        // TODO: display proper error message
        setEmailError("email error");
    }

    @Override
    public void onPasswordValidationError(SignUpModel.PasswordError passwordError) {
        // TODO: display proper error message
        setPasswordError("password error");
    }

    @Override
    public void onConfirmPasswordValidationError() {
        // TODO: display proper error message
        setConfirmPasswordError("password mismatch");
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getConfirmPassword() {
        return confirmPassword;
    }

    @Override
    public String getEmailError() {
        return emailError;
    }

    @Override
    public String getPasswordError() {
        return passwordError;
    }

    @Override
    public String getConfirmPasswordError() {
        return confirmPasswordError;
    }

    private void setEmail(String email) {
        if (email != null && !email.equals(this.email)) {
            this.email = email;
            notifyPropertyChanged(BR.email);
        }
    }

    private void setPassword(String password) {
        if (password != null && !password.equals(this.password)) {
            this.password = password;
            notifyPropertyChanged(BR.password);
        }
    }

    private void setConfirmPassword(String confirmPassword) {
        if (confirmPassword != null && !confirmPassword.equals(this.confirmPassword)) {
            this.confirmPassword = confirmPassword;
            notifyPropertyChanged(BR.confirmPassword);
        }
    }

    private void setEmailError(String emailError) {
        this.emailError = emailError;
        notifyPropertyChanged(BR.emailError);
    }

    private void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
        notifyPropertyChanged(BR.passwordError);
    }

    private void setConfirmPasswordError(String confirmPasswordError) {
        this.confirmPasswordError = confirmPasswordError;
        notifyPropertyChanged(BR.confirmPasswordError);
    }
}
