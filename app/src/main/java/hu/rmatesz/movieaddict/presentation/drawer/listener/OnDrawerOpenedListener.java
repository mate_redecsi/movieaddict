package hu.rmatesz.movieaddict.presentation.drawer.listener;

/**
 * Created by Mate Redecsi on 2016. 10. 27..
 */

public interface OnDrawerOpenedListener {
    void onDrawerOpened();
}
