package hu.rmatesz.movieaddict.presentation.search.viewmodel;

import android.view.View;

import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.presentation.search.router.SearchRouter;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */
public class SeriesItemViewModel extends SearchItemViewModel {
    private final SearchRouter router;
    private Series series;

    public SeriesItemViewModel(SearchRouter router) {
        this.router = router;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    @Override
    public String getPosterUrl() {
        return series.getPoster();
    }

    @Override
    public void onItemClicked(View v) {
//        ActivityOptionsCompat options = ActivityOptionsCompat.
//                makeSceneTransitionAnimation(router.getActivity(), v.findViewById(R.id.poster), "poster");
        router.openSeriesDetails(series, null);
    }

    @Override
    public String getTitle() {
        return series.getTitle();
    }
}
