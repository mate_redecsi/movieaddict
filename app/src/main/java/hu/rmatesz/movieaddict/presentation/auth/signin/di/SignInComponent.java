package hu.rmatesz.movieaddict.presentation.auth.signin.di;

import dagger.Component;
import hu.rmatesz.movieaddict.common.di.ApplicationComponent;
import hu.rmatesz.movieaddict.common.di.PerActivity;
import hu.rmatesz.movieaddict.data.auth.di.AuthenticationDataComponent;
import hu.rmatesz.movieaddict.presentation.auth.signin.router.SignInActivity;

/**
 * Created by Mate Redecsi on 2016. 10. 26..
 */
@PerActivity
@Component(dependencies = {ApplicationComponent.class, AuthenticationDataComponent.class}, modules = {SignInModule.class})
public interface SignInComponent {
    void inject(SignInActivity activity);

    final class Injector {

        public static void inject(SignInActivity activity) {
            ApplicationComponent applicationComponent = ApplicationComponent.Injector.getComponent();
            AuthenticationDataComponent authenticationDataComponent = applicationComponent.authenticationDataComponent().build();

            DaggerSignInComponent.builder()
                    .applicationComponent(applicationComponent)
                    .authenticationDataComponent(authenticationDataComponent)
                    .signInModule(new SignInModule(activity))
                    .build()
                    .inject(activity);
        }
    }
}
