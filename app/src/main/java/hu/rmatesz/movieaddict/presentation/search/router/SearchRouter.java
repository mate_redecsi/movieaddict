package hu.rmatesz.movieaddict.presentation.search.router;

import android.os.Bundle;

import hu.rmatesz.movieaddict.api.model.Series;

/**
 * Created by Mate_Redecsi on 10/22/2016.
 */

public interface SearchRouter {
    void openSeriesDetails(Series series, Bundle bundle);
}
