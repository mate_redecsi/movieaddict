package hu.rmatesz.movieaddict.presentation.auth.signup.model;

/**
 * Created by Mate Redecsi on 2016. 11. 03..
 */

public interface SignUpModel {
    void validateEmail(String email);
    void validatePassword(String password);
    void validateConfirmPassword(String password, String confirmPassword);
    void signUp(String email, String password);
    void stop();
    void registerCallback(Callback callback);
    void unregisterCallback(Callback callback);

    interface Callback {
        void onSuccessfulSignUp();
        void onSignUpError();
        void onEmailValidationError(EmailError emailError);
        void onPasswordValidationError(PasswordError passwordError);
        void onConfirmPasswordValidationError();
    }

    enum EmailError {
        EMPTY,
        INVALID_FORMAT
    }

    enum PasswordError {
        EMPTY,
        TOO_SHORT
    }
}
