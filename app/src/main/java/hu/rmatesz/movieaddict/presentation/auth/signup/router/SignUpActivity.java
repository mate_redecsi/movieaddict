package hu.rmatesz.movieaddict.presentation.auth.signup.router;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import hu.rmatesz.movieaddict.databinding.SignUpActivityBinding;
import hu.rmatesz.movieaddict.presentation.auth.signup.di.SignUpComponent;
import hu.rmatesz.movieaddict.presentation.auth.signup.viewmodel.SignUpViewModel;

/**
 * Created by Mate_Redecsi on 11/12/2016.
 */

public class SignUpActivity extends AppCompatActivity implements SignUpRouter {
    @Inject
    SignUpViewModel viewModel;
    @Inject
    SignUpActivityBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SignUpComponent.Injector.inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.onStart();
    }

    @Override
    protected void onStop() {
        viewModel.onStop();
        super.onStop();
    }
    @Override
    public void openStartPage() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void displayErrorDialog(String title, String message) {

    }

    @Override
    public void displaySnackbarMessage(String message) {
        Snackbar.make(binding.getRoot(), message, Snackbar.LENGTH_LONG).show();
    }
}
