package hu.rmatesz.movieaddict.data.series.provider;

import java.util.List;

import hu.rmatesz.movieaddict.api.model.Series;
import rx.Observable;

/**
 * Created by Mate_Redecsi on 11/13/2016.
 */

public interface SeriesDataProvider {
    Observable<List<Series>> searchSeries(final String keyword);
    void loadPage();
    Observable<Series> getSeriesById(String id);
}
