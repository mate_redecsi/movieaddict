package hu.rmatesz.movieaddict.data.auth.di;

import dagger.Subcomponent;
import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;
import hu.rmatesz.movieaddict.firebase.di.FirebaseModule;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */
@Subcomponent(modules = { AuthenticationDataModule.class, FirebaseModule.class})
public interface AuthenticationDataComponent {
    AuthenticationDataProvider authenticationDataProvider();

    @Subcomponent.Builder
    interface Builder {
        AuthenticationDataComponent.Builder authenticationDataModule(AuthenticationDataModule authenticationDataModule);
        AuthenticationDataComponent.Builder firebaseModule(FirebaseModule module);
        AuthenticationDataComponent build();
    }
}
