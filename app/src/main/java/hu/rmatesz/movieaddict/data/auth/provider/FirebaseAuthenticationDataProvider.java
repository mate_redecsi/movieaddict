package hu.rmatesz.movieaddict.data.auth.provider;

import hu.rmatesz.movieaddict.data.auth.model.User;
import hu.rmatesz.movieaddict.firebase.FirebaseAuthRxDecorator;
import rx.Observable;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */

public class FirebaseAuthenticationDataProvider implements AuthenticationDataProvider {
    private final FirebaseAuthRxDecorator firebaseAuth;

    public FirebaseAuthenticationDataProvider(FirebaseAuthRxDecorator firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    public Observable<User> signIn(String email, String password) {
        return firebaseAuth.signInWithEmailAndPassword(email, password)
                .map(authResult -> new User(authResult.getUser().getEmail(), null));
    }

    @Override
    public Observable<User> signUp(String email, String password) {
        return firebaseAuth.createUserWithEmailAndPassword(email, password)
                .map(authResult -> new User(authResult.getUser().getEmail(), null));
    }

    @Override
    public void signOut() {
        firebaseAuth.signOut();
    }
}
