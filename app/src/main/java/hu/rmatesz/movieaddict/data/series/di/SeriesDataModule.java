package hu.rmatesz.movieaddict.data.series.di;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.api.service.OmdbService;
import hu.rmatesz.movieaddict.data.series.provider.SeriesDataProvider;
import hu.rmatesz.movieaddict.data.series.provider.SeriesDataProviderImpl;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */
@Module
public class SeriesDataModule {
    @Provides
    SeriesDataProvider provideSeriesDataProvider(OmdbService omdbService) {
        return new SeriesDataProviderImpl(omdbService);
    }
}
