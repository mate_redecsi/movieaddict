package hu.rmatesz.movieaddict.data.auth.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */

public class User {
    @NonNull
    private String email;
    @Nullable
    private String password;

    public User(@NonNull String email, @Nullable String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof User)) {
            return false;
        }
        User other = (User) obj;
        return email.equals(other.email) && ((password == other.password) || (password != null && password.equals(other.password)));
    }
}
