package hu.rmatesz.movieaddict.data.movies.di;

import dagger.Subcomponent;
import hu.rmatesz.movieaddict.api.di.ApiModule;
import hu.rmatesz.movieaddict.data.movies.provider.MoviesDataProvider;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */
@Subcomponent(modules = {MoviesDataModule.class, ApiModule.class})
public interface MoviesDataComponent {
    MoviesDataProvider moviesDataProvider();

    @Subcomponent.Builder
    interface Builder {
        MoviesDataComponent.Builder moviesDataModule(MoviesDataModule module);

        MoviesDataComponent build();
    }
}
