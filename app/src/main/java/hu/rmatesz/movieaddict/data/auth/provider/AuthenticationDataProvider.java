package hu.rmatesz.movieaddict.data.auth.provider;

import hu.rmatesz.movieaddict.data.auth.model.User;
import rx.Observable;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */

public interface AuthenticationDataProvider {
    Observable<User> signIn(String email, String password);
    Observable<User> signUp(String email, String password);
    void signOut();
}
