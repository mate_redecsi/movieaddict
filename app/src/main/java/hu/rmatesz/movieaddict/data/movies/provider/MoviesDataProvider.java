package hu.rmatesz.movieaddict.data.movies.provider;

import java.util.List;

import hu.rmatesz.movieaddict.api.model.Movie;
import rx.Observable;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */

public interface MoviesDataProvider {
    Observable<List<Movie>> searchMovies(final String keyword);
    void loadPage();
    Observable<Movie> getMovieById(String id);
}
