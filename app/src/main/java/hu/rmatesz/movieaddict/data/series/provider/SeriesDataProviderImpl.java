package hu.rmatesz.movieaddict.data.series.provider;

import java.util.ArrayList;
import java.util.List;

import hu.rmatesz.movieaddict.api.model.Series;
import hu.rmatesz.movieaddict.api.service.OmdbService;
import hu.rmatesz.movieaddict.api.service.response.SearchResponse;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by Mate_Redecsi on 11/13/2016.
 */

public class SeriesDataProviderImpl implements SeriesDataProvider {
    private final OmdbService omdbService;
    private PublishSubject<Integer> pager;
    int page;

    public SeriesDataProviderImpl(OmdbService omdbService) {
        this.omdbService = omdbService;
    }

    @Override
    public synchronized Observable<List<Series>> searchSeries(final String keyword) {
        pager = PublishSubject.create();
        page = 1;
        List<Series> series = new ArrayList<>();
        return omdbService.searchSeries(keyword, 1)
                .concatWith(pager.observeOn(Schedulers.computation()).concatMap(page -> omdbService.searchSeries(keyword, page)))
                .map(response -> processSearchResponse(response, series))
                .doOnError(e -> --page);
    }

    @Override
    public synchronized void loadPage() {
        pager.onNext(++page);
    }

    @Override
    public Observable<Series> getSeriesById(String id) {
        return omdbService.getSeriesById(id);
    }

    private List<Series> processSearchResponse(SearchResponse<Series> response, List<Series> series) {
        int totalResults = response.getTotalResults();
        List<Series> result = response.getSearch() == null ? new ArrayList<>() : response.getSearch();
        series.addAll(result);

        if (totalResults <= series.size()) {
            pager.onCompleted();
        }

        return result;
    }
}
