package hu.rmatesz.movieaddict.data.movies.di;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.api.service.OmdbService;
import hu.rmatesz.movieaddict.data.movies.provider.MoviesDataProvider;
import hu.rmatesz.movieaddict.data.movies.provider.MoviesDataProviderImpl;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */
@Module
public class MoviesDataModule {
    @Provides
    MoviesDataProvider provideMoviesDataProvider(OmdbService omdbService) {
        return new MoviesDataProviderImpl(omdbService);
    }
}
