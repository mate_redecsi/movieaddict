package hu.rmatesz.movieaddict.data.movies.provider;

import java.util.ArrayList;
import java.util.List;

import hu.rmatesz.movieaddict.api.model.Movie;
import hu.rmatesz.movieaddict.api.service.OmdbService;
import hu.rmatesz.movieaddict.api.service.response.SearchResponse;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by Mate_Redecsi on 12/27/2016.
 */

public class MoviesDataProviderImpl implements MoviesDataProvider {
    private final OmdbService omdbService;
    private PublishSubject<Integer> pager;
    int page;

    public MoviesDataProviderImpl(OmdbService omdbService) {
        this.omdbService = omdbService;
    }

    @Override
    public synchronized Observable<List<Movie>> searchMovies(final String keyword) {
        pager = PublishSubject.create();
        page = 1;
        List<Movie> movies = new ArrayList<>();
        return omdbService.searchMovies(keyword, 1)
                .concatWith(pager.observeOn(Schedulers.computation()).concatMap(page -> omdbService.searchMovies(keyword, page)))
                .map(response -> processSearchResponse(response, movies))
                .doOnError(e -> --page);
    }

    @Override
    public synchronized void loadPage() {
        pager.onNext(++page);
    }

    @Override
    public Observable<Movie> getMovieById(String id) {
        return omdbService.getMovieById(id);
    }

    private List<Movie> processSearchResponse(SearchResponse<Movie> response, List<Movie> movies) {
        int totalResults = response.getTotalResults();
        List<Movie> result = response.getSearch() == null ? new ArrayList<>() : response.getSearch();
        movies.addAll(result);

        if (totalResults <= movies.size()) {
            pager.onCompleted();
        }

        return result;
    }
}
