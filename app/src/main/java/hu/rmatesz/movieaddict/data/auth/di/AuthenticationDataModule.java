package hu.rmatesz.movieaddict.data.auth.di;

import dagger.Module;
import dagger.Provides;
import hu.rmatesz.movieaddict.data.auth.provider.AuthenticationDataProvider;
import hu.rmatesz.movieaddict.data.auth.provider.FirebaseAuthenticationDataProvider;
import hu.rmatesz.movieaddict.firebase.FirebaseAuthRxDecorator;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */
@Module
public class AuthenticationDataModule {
    @Provides
    AuthenticationDataProvider provideFirebaseAuthenticationDataProvider(FirebaseAuthRxDecorator firebaseAuth) {
        return new FirebaseAuthenticationDataProvider(firebaseAuth);
    }
}
