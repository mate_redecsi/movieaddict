package hu.rmatesz.movieaddict.data.series.di;

import dagger.Subcomponent;
import hu.rmatesz.movieaddict.api.di.ApiModule;
import hu.rmatesz.movieaddict.data.series.provider.SeriesDataProvider;

/**
 * Created by Mate_Redecsi on 12/24/2016.
 */
@Subcomponent(modules = {SeriesDataModule.class, ApiModule.class})
public interface SeriesDataComponent {
    SeriesDataProvider seriesDataProvider();

    @Subcomponent.Builder
    interface Builder {
        SeriesDataComponent.Builder seriesDataModule(SeriesDataModule module);

        SeriesDataComponent build();
    }
}
